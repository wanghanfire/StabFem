
% !TEX root = main.tex

\chapter{Basic drivers and explanation of the interface}



The purpose of this chapter is to demonstrate the usage of \stabfem as a Matlab/Octave interface to \freefem, 
allowing to monitor computations, import and plot results from Matlab/Octave.

The chapter will present the fundamental \stabfem drivers, namely \SF{Mesh},  \SF{Launch},  \SF{Plot} and \SF{ExtractData}, and will explain the usage of the exchange files  (.ff2m) allowing importation of the results.

For the purpose of the demonstration, we focus on four simple physical problems formulated in a L-shaped closed domain. All the operations are enclose in the  tutorial example \mlfile{EXAMPLE\_Lshape.m} which is published on the website of the project. A simplified (but still operational) version if this script is provided in figure \ref{fig:SCRIPT_Lshape.m}.

The reader is encouraged to study the code in parallel to the lecture of this chapter, and to try by his own all the steps. 

%This directory contains three freefem programs and a demo matlab script doing solving the following problem :

\begin{figure}
\includegraphics[width = .45 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Lshape_T0.png}
\includegraphics[width = .45 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Lshape_T0_Cut.png}
\caption{Solution for the steady heat equation in a L-shape domain : $(a)$ Temperature field $T(x,y)$. 
$(b)$ Temperature $T(x,0.25)$ along a horizontal line. }
\label{Lshape_Problem1}
\end{figure}

\begin{figure}
\setlistmatlab
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape_summary.m}
\caption{
Matlab program \mlfile{SCRIPT\_Lshape.m} (simplified version)}
\label{fig:SCRIPT_Lshape.m}
\end{figure}


\section{Physical problems and results}



After initialization (lines 5-6 of the script) and generation of a mesh (lines 9-10), the four following problems are successively solved:

\subsubsection*{Physical problem 1} 
Steady heat conduction on a L-shaped domain with constant volumic source term $P$ and homogeneous Dirichlet boundary conditions. 

$$ \nabla^2 T = P  \mbox {  for } {\bf x} \in \Omega ; \quad T = 0   \mbox {  for } {\bf x} \in \Gamma$$

This problem is solved on lines 13-18 of the script and the results are displayed in figure \ref{Lshape_Problem1}.

\begin{figure}
\includegraphics[width = .32 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Lshape_Tc.png}
\includegraphics[width = .32 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Z_omega.png}
\includegraphics[width = .32 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/StokesFlow.png}
\caption{$(a)$ Solution for the unsteady heat equation in a L-shape domain for $\omega =100$. $b$ Thermal impedance for  $\omega \in [0.1-10000]$. $c$ Stokes flow.}
\label{Lshape_problem2}
\end{figure}

\subsubsection*{Physical problem 2} Unsteady heat conduction equations on a L-shaped domain with zero source term $P$ and time-periodic Dirichlet boundary conditions.

$$ 
 \partial T/\partial t= \nabla^2 T  \mbox {  for } {\bf x} \in \Omega ; \quad T = T_w \cos (\omega t )   \mbox {  for } {\bf x} \in \Gamma
$$

The problem can be solved in the frequency domain by setting 
$T(x,y,t) = Re \left( \hat{T}(x,y) e^{-i \omega t} \right)$ , leading to: 
$$ 
 - i \omega \hat{T}= \nabla^2 \hat{T}  \mbox {  for } {\bf x} \in \Omega ; \quad \hat{T} = T_w    \mbox {  for } {\bf x} \in \Gamma
$$

This problem is solved on lines 21-27 of the script and the results for $\omega = 100$ are displayed in figure \ref{Lshape_problem2}$a$.





\subsubsection*{Physical problem 3} Thermal impedance of the L-shaped domain as function of $\omega$.

This problem is the same as the previous one, except that instead of plotting $T$ for a given value of $\omega$ we want to characterize the behavior in the unsteady regime by defining the { \em Thermal impedance} corresponding to

$$ Z_\theta = \frac{T_w}{\hat{\phi}} $$ 

where $\hat \phi$ is the complex heat flux given by $\hat \phi = \int_\partial \Omega \nabla \hat{T} \cdot {\bf n} $. The program computes $Z_\theta$ in the range $\omega  \in [10^{-1} - 10^4]$ and plots the results in logarithmic coordinates.
The problem is solved on lines 29-32 of the scripts and results are shown in figure \ref{Lshape_problem2}$b$.



\subsubsection*{Physical problem 4} Stokes flow with lid-driven cavity.

Here we compute the velocity field ${\bf u} = [u_x,u_y]$ and the pressure field $p$ of a viscous flow in the Stokes regime, with imposed velocity at the bottom boundary $\Gamma_b$:
 $$
 - \nabla p + \Delta {\bf u} =0 ;\quad \nabla \cdot {\bf u} = 0
 $$
 
  ${\bf u} = x(1-x) {\bf e}_x$ on bottom, and ${\bf u} = {\bf 0}$ on other boundaries.
  

The problem is solved on lines 24-38 of the scripts and results are shown in figure \ref{Lshape_problem2}$c$.




\section{Usage of the drivers}

In this section, we explain how the drivers are used to interface the various \freefem programs entering this example, \fffile{Lshape\_Mesh.edp}, \fffile{Lshape\_Steady.edp}, etc... and to plot the results.

The \freefem programs are sufficiently short to be fully given in these pages. See figures \ref{Lshape_Mesh.edp}, \ref{Lshape_Steady.edp} and subsequent. The reader already familiar with FreeFm++ should not have any problem in understanding the programs. If not, we recommand that you study the \freefem Manual (chapter "Learning by examples").


\subsection{ Mesh generation : driver \SF{Mesh}}
\label{sec:MeshBasic}

Let us explain in more detail the very first step of nearly all \stabfem computations : Generation of a mesh. This step is handled using the fundamental driver \SF{Mesh}. 

As can be seen on line  10 , the first argument of this function is the name of a valid \freefem script generating the mesh ; here  \fffile{Lshape\_Mesh.edp}. The next arguments are optional argument as couples of descriptors-values. Here two optional arguments are specified.

The first one, \mlcode{'Params'}, specifies a parameter to be passed as input paramer to the FreeFem program.
In the present case, the sequence is equivalent to launching \shell{echo 40 | FreeFem++ Lshape\_Mesh.edp}\footnote{Here and in the}  when using FreeFem in shell terminal mode.

The second parameter \mlcode{'problemtype'}, specifies the class of problems for which the mesh is designed. The value used here \mlcode{'EXAMPLE'} is not important, but as will be seen in the next chapters, the value of this parameter is used by higher-order drivers to select the right solvers to use.

\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Mesh.edp}
\caption{ \freefem program \fffile{Lshape\_Mesh.edp}. }
\label{Lshape_Mesh.edp}
\end{figure*}


The Freefem code \fffile{Lshape\_Mesh.edp} is displayed in figure \ref{Lshape_Mesh.edp} (up). This program generates a output file \fffile{mesh.msh} in the usual \freefem format, as well as two additional files in .ff2m format, generated by two macros (defined in a file \fffile{Macros\_StabFem.idp} included at the beginning). The significance of these exchange files will be explained in a subsequent chapter.


\Remark{ 
This FreeFem program, and the next ones of the present chapters, require a few macros (here \ffcode{SFWriteMesh},  \ffcode{SFWriteConectivity}). In the pedagogical example of this chapter, these macros (and a few others) are defined in a file \fffile{Macros\_StabFem.idp} located in the working directory. In the more advanced examples of the next chapters, such macros are either in a common file \fffile{SOURCES\_FREEFEM/INCLUDE/SF\_Tools.idp} (for generic macros) or in a local file \fffile{SF\_Custom.idp} (for customizable macros).
}



%Running this program through the driver \SF{Mesh} produces the result displayed in figure  \ref{Lshape_Mesh.edp} (bottom). As can be seen, the result is a Matlab/Octave structure with a number of field, including array of points, descriptors, etc... These fields will serve for plotting data obtained on this mesh. See more in section ??.



\subsection{ Running a FreeFem computation using a mesh : driver \SF{Launch}}
\label{sec:SFLaunch}


 The second step is {\bf launching a FreeFem program and importing results}. 
Both these steps are done using the generic driver function \SF{Launch} which is invoked four times in the main script. As can be seen, this driver is used in the same way as the previous one, i.e. the first argument is a valid \freefem program and the sequel is formed by pairs of descriptor/value optional arguments. The possible arguments are:


\begin{itemize}
\item \mlcode{'Mesh'} is used to specify the mesh on which the calculation will be performed. 
The value is a previously generated Mesh structure, and the corresponding .msh file will be automatically positioned for correct operation.

\item  \mlcode{'Datafile'} specifies the name of the result files to be imported. The latter are expected as a couple of .txt / .ff2m files, only the name of the .ff2m file is expected. Note that this parameter is not specified for the first call to \fffile{Lshape\_Steady.edp}, in which case the default value 'Data.ff2m' is used.
 
\item  \mlcode{'Params'} is used to specify the input parameters needed by the FreeFem solver. This feature is used when when invoking \fffile{Lshape\_Unsteady.edp} which requires a single parameter, namely the frequency.
Note that the value may be an array of real values. For instance \SF{Launch('Mysolver.edp','Params',[1 2 3])} will result in launching \shell{echo 1 2 3 | FreeFem++ Mysolver.edp} in shell terminal mode.
 
\item  \mlcode{'Arguments'} (not used in the present examples) is an alternative method to pass arguments to the FreeFem program using the \ffcode{getARGV} freefem command (see Freefem manual for explanations). 
For instance \SF{Launch('Mysolver.edp','Arguments','-Re 100 -M .1')} will result in launching \shell{FreeFem++ Mysolver.edp -Re 100 -M .1} in shell terminal mode.
\end{itemize}

\Remarks{ 
\item In this simple explanation, the \freefem executable is assumed to be \shell{FreeFem++} and to be available in the current path. In reality, the executable can be specified (the default being \shell{FreeFem++-mpi} if available with the installed version of \freefem ) and the corresponding path is automatically detected. These initial settings are managed by \mlfile{SF\_core\_start} which is expected to be invoked at the beginning of every \stabfem script.

\item Note that the two successive actions of \SF{Launch}, namely launching FreeFem++ and importing results, are performed by functions \mlfile{SF\_core\_freefem} and  \mlfile{SFcore\_ImportData}.  You may look inside these functions to understand their operation, but it is not advised to use them directly in a script.


}




%%%%%%%%%%%%%%%%
\subsection{ Plotting results : driver \SF{Plot}}
\label{sec:PlotBasic}

The next important step is plotting the results. A variety of plots can be produced in StabFem, and all of them are managed by the unique driver \mlcode{SF\_Plot}. Examination of the script shows that this driver has to be used with the following set of arguments:

\begin{enumerate}

\item First argument is a valid dataset structure or mesh structure.

\item Second argument is the name of a field of the dataset (possibly followed by the suffixes '.re' or '.im' to select the real or imaginary parts of a complex field) or the keyword 'mesh' (to plot mesh only). Note that we can also specify a cell-array of two names (e.g. \mlcode{\{'ux','uy'\} } in example 4) in which case a vector field is plotted.

\item The next arguments are optional ones passed as pairs or descriptor/value. The options used here comprise:

\begin{itemize}
\item \mlcode{'contour'} to plot isocontours in addition to color levels (with value 'on') or instead of color levels (with value 'only').

\item \mlcode{'xlim'} and  \mlcode{'ylim'}  to specify ranges,

\item \mlcode{'title'} to specify a title,

\item \mlcode{'colormap'} to specify the color map,

\item (...) 

\end{itemize}

A large number of other options are available and a full list will not be given here. 
The user is encouraged to explore the numerous examples of the project to learn about all possibilities.
A documentation can also be generated using \mlcode{'help SF\_Plot' } .


\end{enumerate}

\Remark{ The driver \SF{Plot} is built upon the function \mlcode{ffpdeplot} designed by Markus Meisinger. See the website of this project for more documentation.}


\subsection{ Extracting results from mesh-associated data : driver \SF{ExtractData}}

For post-processsing your results, you will certainly need to access to the value of the mesh-associated fields at a given point, or a given series of points. 
The driver \SF{ExtractData} is designed for this purpose. This function is called at line ?? of our main script. We can see that the general usage is as follows:

\mlcode{ values = SF\_ExtractData(bf,field,x,y) }

Where :
\begin{itemize}
\item \mlcode{bf} is a valid dataset.
\item \mlcode{field} is the name of a mesh-associated field of this dataset (here \mlcode{'T'}).
\item \mlcode{x} and \mlcode{y} are the coordinated on which the field is to be interpolated.  \mlcode{x} and \mlcode{y} are normally expected as arrays with same dimensions ; the return value \mlcode{values} will have the same dimension.
\end{itemize}

Note that the routine will also work if \mlcode{x} is an array and \mlcode{y} is a single value; in this case the extraction will be done on a horizontal line.
Same for a vertical line if \mlcode{y} is an array and \mlcode{x} is a single value.

Note that the driver \SF{ExtractData} is built upon the function \mlcode{ffinterpolate} designed by Markus Meisinger. Part of this function contains a .mex file which can be compiled, leading to substantial speed-up of the routine. See the documentation on the website of ffpdeplot about this (link ?).

In your work you will certainly need some post-processing results which are not easily extracted from the mesh-associated data (e.g. the heat flux across the whole boundary for the thermal problems, the force exerted on the moving wall for the Stokes flow problem, etc...). This second kind of pos-processing has to be incorporated directly in the FreeFem codes and the importation will be done from the  .txt/.ff2m. exchange files. 
This will be the object of the next section.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{How does is work ?}


\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Steady.edp}
\setlistmatlab
\lstinputlisting[linerange={25-37}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Steady.edp}(up) and result of the execution of this program through the driver\SF{Launch} (bottom).  }
\label{Lshape_Steady.edp}
\end{figure*}




The FreeFem++ programs for the four elementary problems as well as the result of their execution through \SF{Launch} are presented in figures \ref{Lshape_Steady.edp}-\ref{Lshape_Stokes.edp}.

As can be seen all the files produce a couple of \shell{.txt/.ff2m} files which are imported by the drivers.
The role and structure of these files are as follows:
\begin{itemize}
\item The \shell{.txt} contains the main results of the computation exported under the default (ascii) output mode of FreeFem++, namely the mesh-associated fields (here \ffcode{T,u,v,p}) and/or scalar or vectorial numerical data (here \ffcode{omega,Zr,Zi}. These files can thus be subsequently used either as input files for next FreeFem++ computations, or for importation by StabFem.

\item The  \shell{.ff2m} may contain {\em Explanations for the drivers on how to import/interpret the \shell{.txt} files}, plus  {\em auxiliary data} not contained in the .txt file but useful for post-processing, and  {\em metadata} useful for indexing the data. 
 \end{itemize}
 
 
\subsection{Explanation of the \shell{.ff2m} exchange format} 

The \shell{.ff2m} format is an exchange format specifically designed for StabFem. As can be inferred from the examples, the general structure of such files is as follows:



\setlistfreefem
\lstinputlisting{Lshape_FormatExplanation.ff2m}.

It is thus constituted of a four-lines header, optionally followed by a set of numerical data. The signification of the header is as follows :

\begin{itemize}
\item Line 1 is skipped.
\item Line 2 contains a description string which is imported as field 'datadescription'.
\item Line 3 contains a list of string-valued keywords coming by pairs of descriptors/values 
{\em explaining how to interpret the .txt file}. \ffcode{datatype} specifies the kind of data (this parameter may affect the operation of some of the drivers). The two next keywords are explained in next paragraph.
The signification of the next keywords \ffcode{datastoragemode} \ffcode{datadescriptors} 
\item Line 4 gives the list of auxiliary data or metadata variables, coming by paits of variable type/variable name. 
\item The sequel contains the numerical data forming the auxiliary and metadata, provided as a unique array of real values.
\end{itemize}

\subsubsection{Importation main data from the .txt file} 

The importation of the .txt file depends on the keywords  \ffcode{datastoragemode} \ffcode{datadescriptors} provided on line 3. Their significance vary depending if the data is {\em mesh-associated} or {\em non-mesh associate}.

\begin{itemize}

\item For {\bf Non-mesh-associated data}, \ffcode{datastoragemode} is obligatorily \ffcode{'columns'} meaning that data in .txt is organized in columns. Then the keywords \ffcode{datadescriptors} is a list of descriptors separated by commas specifying the names under which the data will be imported.

For instance \ffcode{datastoragemode columns datadescriptors Re,Fx} means the data is organized as two columns which will be imported as arrays \mlcode{Re} and \mlcode{Fx}. 

Note in this mode the data in the .txt file must contain only real data, so complex data have to be separated as real and imaginary parts. if the list of descriptors contains a pair of names with \ffcode{\_r} and \ffcode{\_i}  then the driver will reassociate the data as a complex one. 

For instance \ffcode{datastoragemode columns datadescriptors omega,Z\_r,Z\_i} means that the .txt file contains 3 columns and that the two latter will be reassembled as a complex array with name \mlcode{Z}.



\item For {\bf Mesh-associated data},  \ffcode{datastoragemode} contains a prefix (\ffcode{Re} or \ffcode{Cx})  explaining if the data are real or complex, followed by a description of the finite elements used (\ffcode{P1},\ffcode{P2},...), and possibly a suffix \ffcode{'.(n)'}. This suffix explains that the \shell{.txt} file contains a number $n$ of scalar values (real or complex) to be read after the mesh-associated data. Then the keyword \ffcode{datadescriptors} is a list of descriptors separated by commas specifying the name under which the data (mesh-associated ones and extra scalars) 



For instance, \ffcode{datastoragemode CxP2 datadescriptors Tc}  means that the .txt file contains a complex mesh-associated field defined using P2 elements which will be imported under the name \mlcode{Tc}.

 As a second example  \ffcode{datastoragemode ReP2P2P1.1 datadescriptors ux,uy,p,Re} means that the .txt file contains a (real) vectorized field whose components (stored as P2,P2,P1) will be imported under names \mlcode{ux}, \mlcode{uy} and \mlcode{p} plus an additional scalar which will be imported under name \mlcode{Re}.  

Note that unlike for non-mesh-associated data, complex data as expected to we written in the .txt file under the form \ffcode{(re,im)} following the default output mode of \freefem. If present, additional scalar datas are treated in the same way (i.e. if the main data is complex, so are the scalars).


\subsubsection{Importation of 'auxiliary data' and 'metadata' from the .ff2m file} 

auxiliary data and metadata are defined by pairs of type/names on line 4 of the header. There are four kinds of data :

\begin{enumerate}

\item {\em Scalar auxiliary data} comprises the following types :

\ffcode{ real, complex, int }

They will be imported as simple scalars identified with the specifieds name.


\item {\em Scalar metadata} are specified as follows:
\ffcode{ real*, complex*, int* }

They are handled as the simple scalar data, except that they are used by the high-level drivers to index the data and allow  
powerful exploration of the database while postprocessing.

\item {\em Vectorial auxiliary data} are specified as follows:

\ffcode{ real.(N), complex.(N), int.(N) } 

They are imported as arrays  under the specified name.

\item{ Mesh-associated auxiliary data} are extra field derived from the main data which are potentially useful for post-processing but are not needed for subsequent calls to FreeFem (hence they do not need to be in the .txt file).

The following types are currently available:
\begin{itemize}
\item \ffcode{P1},  \ffcode{P1b} and  \ffcode{P2}  refer to standard finite-element formats (for real field).
\item \ffcode{P1c},  \ffcode{P1bc} and  \ffcode{P2c} are the analogous for complex fields.
\item \ffcode{P1surf} for real data defined along a frontier of the mesh (for free-surface cases),
\item \ffcode{P1surfc} same for complex data.
\end{itemize}

The reader is invited to study the examples in figures \ref{Lshape_Unsteady.edp}-\ref{Lshape_Stokes.edp} which will help to understand the link between the content of the \shell{.ff2m} files and the resulting matlab/octave  structures.

\Remark{
The importation of data from \shell{.txt/.ff2m} files is actually done by the \stabfem driver \mlfile{SFcore\_ImportData.m}. This function is called internally within \mlfile{SF\_Launch}, but is  not expected to be directly called within a high-level \stabfem script.
}


\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Unsteady.edp}
\setlistmatlab
\lstinputlisting[linerange={42-58}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Unsteady.edp} (up) and result of the execution of this program through the driver\SF{Launch} (bottom).}
\label{Lshape_Unsteady.edp}
\end{figure*}

\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Impedance.edp}
\setlistmatlab
\lstinputlisting[linerange={62-75}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Impedance.edp} (up) and result of the execution of this program through the driver\SF{Launch} (bottom).}
\label{Lshape_Impedance.edp}
\end{figure*}

\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Stokes.edp}
\setlistmatlab
\lstinputlisting[linerange={77-93}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Stokes.edp} (up) and result of the execution of this program through the driver\SF{Launch} (bottom).s}
\label{Lshape_Stokes.edp}
\end{figure*}

\iffalse
\subsection{Explanations about the mesh }

Examination of the simple example here shows that the mesh generation produces four files (here all generated by the macro SF\_writemesh defined
in file Macros\_StabFem.edp)

\begin{itemize}
\item file \mlfile{mesh.txt} contains the mesh in the native FreeFem format. Namely, information about the vertices, the boundaries and the triangles. This mesh is imported in stabfem using importFFmesm.m , and it is also needed for subsequent \freefem programs.


\item file \mlfile{mesh.ff2m} contains informations about the mesh structure, to be imported by StabFem (it is read by importFFmesm.m)

\item file  \mlfile{SF\_Init.ff2m} contains informations about the mesh geometry, to be imported by StabFem as well  (it is also read by importFFmesm.m)

\item file \mlfile{SF\_geom.edp} contains definitions of case-dependent geometrical parameters which may be used by the \freefem solvers.
This file is designed to be {\em included } in the header of the \freefem solvers.
\end{itemize}

Note that files \mlfile{mesh.ff2m} and  \mlfile{SF\_Init.ff2m} may seem redundant... the difference is that  \mlfile{SF\_Init.ff2m} is created only once 
while  \mlfile{mesh.ff2m} is recreated each time the mesh is modified (adapted, splitted, etc...) .
\fi










