% Resolve la probleme diffusif dans le cas rectangulaire avec une condition 
% 
% 


addpath('../../SOURCES_MATLAB/')
SF_Start('verbosity',4);

r0 = 0.01;
rcol = .1;
L = 1;
Pe=10;
ffmesh = SF_Mesh('Mesh_rectangle_neumann.edp','Params',[r0, rcol, L],'problemtype','Kaptsov_diff');
bf = SF_BaseFlow(ffmesh,'Pe',Pe);

figure; SF_Plot(bf,'phi');
pause(0.1);

%rline = [r0:0.005:L];
%philine = SF_ExtractData(bf,'phi',rline,0)
%figure; plot(rline,philine);

bfNew = SF_Adapt(bf,'Hmax',.1);

figure; subplot(2,1,1); SF_Plot(bf,'mesh')
subplot(2,1,2); SF_Plot(bfNew,'mesh')

figure; SF_Plot(bf,'phi');
figure; SF_Plot(bf,'n');
pause(0.1);
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=2;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=4;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=8;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=16;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=64;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=128;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=256;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=512;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% Pe=1024;
% bf = SF_BaseFlow(ffmesh,'Pe',Pe);
% figure; SF_Plot(bf,'phi');
% figure; SF_Plot(bf,'n');
