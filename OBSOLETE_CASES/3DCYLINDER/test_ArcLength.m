close all;
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',5,'workdir','./WORK/'); 
figureformat='png'; 

ffmesh = SF_Mesh('Mesh_refined.edp','Params',[-40 80 40],'problemtype','2d');
bf=SF_BaseFlow(ffmesh,'Re',1,'Omegax',4.5);
%bf = SF_Adapt(bf);
for Re = [10, 60, 100, 170]
    bf=SF_BaseFlow(bf,'Re',Re,'Omegax',4.5);
end
%bf = SF_Adapt(bf);

%[ev,em] = SF_Stability(bf,'k',0.00001,'shift',0.1,'nev',10);% we take nev = 10 to make sure the first computation will be good... -> now made outside
%[ev2D,em2D] = SF_Stability(bf,'k',0.,'shift',0.1,'nev',10);% we take nev = 10 to make sure the first computation will be good... -> now made outside

figure(12);
subplot(1,2,1); plot(bf.Omegax,bf.Fy,'bx');hold on;xlabel('alpha');ylabel('Fy');
subplot(1,2,2); plot(bf.Omegax,bf.Fx,'bx');hold on;xlabel('alpha');ylabel('Fx');

bf=SF_BFContinue(bf,'step',0.05);
subplot(1,2,1); plot(bf.Omegax,bf.Fy,'rx');
subplot(1,2,2); plot(bf.Omegax,bf.Fx,'rx');
subplot(1,2,1); plot(bf.Omegax+0.05*bf.tangent1,bf.Fy+0.05*bf.tangent2,'o+');



%bf=SF_BaseFlow(bf,'Re',100,'Omegax',5.202);
%[ev,em] = SF_Stability(bf,'k',0.00001,'shift',0.05,'nev',5);% we take nev = 10 to make sure the first computation will be good... -> now made outside

%bf=SF_BFContinue(bf,'step',0.02);
% If jump due to condition in alpha then change step