%%  NONLINEAR STABILITY ANALYSIS of the wake of a rotating of a cylinder with STABFEM  
%

%% ANALYSIS OF THE SADDLE NODE BIFURCATON - Normal form

disp(' ');
disp('######     ENTERING NONLINEAR PART       ####### ');
disp(' ');
close all;
addpath('../../SOURCES_MATLAB/');
verbosity=4;ffdatadir='./WORK/';
SF_Start; 
% Solving some bugs (fast and dirty)
%ffmesh = SF_Mesh('Mesh_refined.edp','Params',[-40 80 40],'problemtype','2D','symmetry','N');
ffmesh = SFcore_ImportMesh('./WORK/mesh.msh','problemtype','2D');
ffmesh.symmetry = 'N';
% end of dirty work
%% DETERMINATION OF THE INSTABILITY THRESHOLD
% Identify the saddle node solution with arc-length
alpha = 5.1 % Threshold for Re=170
disp('COMPUTING INSTABILITY THRESHOLD');
bf=SF_BaseFlow(ffmesh,'Re',1,'Omegax',alpha);
bf=SF_BaseFlow(bf,'Re',20,'Omegax',alpha);
bf=SF_BaseFlow(bf,'Re',100,'Omegax',alpha);
% Verify that we are in the unstable focus
[ev,em] = SF_Stability(bf,'shift',0.3,'nev',3,'type','D');
% Continue up to the rotation rate where saddle node exists
bf=SF_BFContinue(bf,'step',0.2);
bf=SF_BFContinue(bf,'step',0.2);
bf=SF_BFContinue(bf,'step',0.1);
bf=SF_BFContinue(bf,'step',0.1);
bf=SF_BFContinue(bf,'step',0.1);
bf=SF_BFContinue(bf,'step',0.04);
bf=SF_BFContinue(bf,'step',0.05);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
% We have converged towards the saddle node
% Now we look for the first fold
bf=SF_BaseFlow(bf,'Re',100,'Omegax',5.18);
bf=SF_BFContinue(bf,'step',-0.05);
bf=SF_BFContinue(bf,'step',-0.05);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
bf=SF_BFContinue(bf,'step',-0.05);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
bf=SF_BFContinue(bf,'step',0.002);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
bf=SF_BFContinue(bf,'step',0.005);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
% Computed threshold
ev = ev(1);
em = em(1);
Rec = bf.Re;  Fxc = bf.Fx; 
Lxc=bf.Lx;    Omegac=imag(em.lambda);

[ev,em] = SF_Stability(bf,'shift',1i*Omegac,'nev',1,'type','S'); % type "S" because we require both direct and adjoint
[wnl] = SF_WNL(bf,em,'NormalForm','SaddleNode'); 

%% Compute Saddle node at TB (or not that far)
bf=SF_BaseFlow(bf,'Re',78.0,'Omegax',5.353);
bf=SF_BFContinue(bf,'step',0.005);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
Omegac=imag(em(1).lambda);
[ev,em] = SF_Stability(bf,'shift',1i*Omegac,'nev',1,'type','S'); % type "S" because we require both direct and adjoint
[wnlTB] = SF_WNL(bf,em,'NormalForm','SaddleNode'); 
% It is not far, but not close enough | still both coeff != 0
bf=SF_BaseFlow(bf,'Re',77.5,'Omegax',5.358);
bf=SF_BFContinue(bf,'step',0.005);
bf=SF_BFContinue(bf,'step',0.005);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
bf=SF_BFContinue(bf,'step',0.001);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
bf=SF_BFContinue(bf,'step',0.0005);
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',3,'type','D');
[ev,em] = SF_Stability(bf,'shift',1i*imag(ev(1)),'nev',1,'type','S'); % type "S" because we require both direct and adjoint
[wnlTB] = SF_WNL(bf,em,'NormalForm','SaddleNode'); 
% a0 becomes 0

