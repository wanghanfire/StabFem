%%  NONLINEAR STABILITY ANALYSIS of the wake of a rotating of a cylinder with STABFEM  
%

%% CHAPTER 4 : computation of weakly nonlinear expansion

disp(' ');
disp('######     ENTERING NONLINEAR PART       ####### ');
disp(' ');
close all;
addpath('../../SOURCES_MATLAB/');
verbosity=4;ffdatadir='./WORK/';
SF_Start; 
% Solving some bugs (fast and dirty)
%ffmesh = SF_Mesh('Mesh_refined.edp','Params',[-40 80 40],'problemtype','2D','symmetry','N');
ffmesh = SFcore_ImportMesh('./WORK/mesh.msh','problemtype','2D');
ffmesh.symmetry = 'N';
% end of dirty work
%% DETERMINATION OF THE INSTABILITY THRESHOLD
% Rotation speed
alpha = 5.565 % Threshold for Re=170
disp('COMPUTING INSTABILITY THRESHOLD');
bf=SF_BaseFlow(ffmesh,'Re',1,'Omegax',alpha);
bf=SF_BaseFlow(bf,'Re',20,'Omegax',alpha);
bf=SF_BaseFlow(bf,'Re',50,'Omegax',alpha);

[ev,em] = SF_Stability(bf,'shift',0.146i,'nev',1,'type','D');
Rec = 50.0; % Change this to compute directly but in this case ...
ReBeyond = 50.25;

Rec = bf.Re;  Fxc = bf.Fx; 
Lxc=bf.Lx;    Omegac=imag(em.lambda);

[ev,em] = SF_Stability(bf,'shift',1i*Omegac,'nev',1,'type','S'); % type "S" because we require both direct and adjoint
[wnl,meanflow,mode] = SF_WNL(bf,em,'Retest',ReBeyond); 

%% 
% PLOTS of WNL predictions

epsilon2_WNL = -0.003:.0001:.005; % will trace results for Re = 40-55 approx.
Re_WNL = 1./(1/Rec-epsilon2_WNL);
A_WNL = wnl.Aeps*real(sqrt(epsilon2_WNL));
Fy_WNL = wnl.Fyeps*real(sqrt(epsilon2_WNL))*2; % factor 2 because of complex conjugate
omega_WNL =Omegac + epsilon2_WNL*imag(wnl.Lambda) ...
                  - epsilon2_WNL.*(epsilon2_WNL>0)*real(wnl.Lambda)*imag(wnl.nu0+wnl.nu2)/real(wnl.nu0+wnl.nu2)  ;
Fx_WNL = wnl.Fx0 + wnl.Fxeps2*epsilon2_WNL  ...
                 + wnl.FxA20*real(wnl.Lambda)/real(wnl.nu0+wnl.nu2)*epsilon2_WNL.*(epsilon2_WNL>0) ;

figure(20);hold on;
plot(Re_WNL,real(wnl.Lambda)*epsilon2_WNL,'g--','LineWidth',2);hold on;

figure(21);hold on;
plot(Re_WNL,omega_WNL/(2*pi),'g--','LineWidth',2);hold on;
xlabel('Re');ylabel('St');

figure(22);hold on;
plot(Re_WNL,Fx_WNL,'g--','LineWidth',2);hold on;
xlabel('Re');ylabel('Fx');

figure(24); hold on;
plot(Re_WNL,abs(Fy_WNL),'g--','LineWidth',2);
xlabel('Re');ylabel('Fy')

figure(25);hold on;
plot(Re_WNL,A_WNL,'g--','LineWidth',2);
xlabel('Re');ylabel('AE')

pause(0.1);



%% CHAPTER 5 : SELF CONSISTENT / HB1

disp('SC quasilinear model on the range [Rec , 100]');
Re_HB = [51:1:200];
alpha_HB = linspace(5.56,4.35,length(Re_HB));

Re_HB = [85:5:200];
alpha_HB = linspace(5.355,4.35,length(Re_HB));

% THE STARTING POINT HAS BEEN GENERATED ABOVE, WHEN PERFORMING THE WNL ANALYSIS
Res = 50.25 ; 

 Lx_HB = [Lxc]; Fx_HB = [Fxc]; omega_HB = [Omegac]; Aenergy_HB  = [0];  Aenergy2_HB  = [0]; Fy_HB = [0];

%[meanflow,mode] = SF_HB1(meanflow,mode,'sigma',0.,'Re',Res); 
meanflow = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/MeanFlow_Re83_Omegax5.3001.ff2m');
mode = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/Harmonic1_Re83_Omegax5.3001.ff2m');
mode2 = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/Harmonic2_Re83_Omegax5.3001.ff2m');

[meanflow,mode,mode2] = SF_HB2(meanflow,mode,'Omegax',meanflow.Omegax); 
%% Loop until Re=200
for i = [1:length(Re_HB)]
    Re = Re_HB(i);
    alpha = alpha_HB(i);
    [meanflow,mode,mode2] = SF_HB2(meanflow,mode,mode2,'Re',Re,'Omegax',...
                             alpha,'symmetry','N','symmetryBF','N');  
    Lx_HB = [Lx_HB meanflow.Lx];
    Fx_HB = [Fx_HB meanflow.Fx];
    omega_HB = [omega_HB imag(mode.lambda)];
    Aenergy_HB  = [Aenergy_HB mode.AEnergy];
    Aenergy2_HB  = [Aenergy_HB mode2.AEnergy];
    Fy_HB = [Fy_HB mode.Fy];
end

%%

%% Explore supercritical limit cycle (double)
%  Lx_HB = []; Fx_HB = []; omega_HB = []; Aenergy_HB  = []; Fy_HB = [];
% alpha_List = [4.34:-0.01:4.1]
% for i = [1:length(alpha_List)]
%     alpha = alpha_List(i);
%     [meanflow,mode] = SF_HB1(meanflow,mode,'sigma',0.,'Re',200,'Omegax',...,
%         alpha,'symmetry','N','symmetryBF','N'); 
% end
% meanflow = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/MeanFlow_Re50.25.ff2m');
% mode = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/HBMode1_Re50.25.ff2m');
% mode2 = SFcore_ImportData(ffmesh,'./WORK/HBMode2_guess.ff2m');
% 
% [meanflowHB2,modeHB2,mode2HB2] = SF_HB2(meanflow,mode,'Re',200);
% 
% 
% save('Cylinder_AllFigures.mat');
% 

%% chapter 5b : figures
%load('Cylinder_AllFigures.mat');

% figure(21);hold off;
% plot(Re_LIN,imag(lambda_LIN)/(2*pi),'b+-');
% hold on;
% plot(Re_WNL,omega_WNL/(2*pi),'g--','LineWidth',2);hold on;
% plot(Re_HB,omega_HB/(2*pi),'r-','LineWidth',2);
% plot(Rec,Omegac/2/pi,'ko');
% LiteratureData=csvread('./literature_data/fig7a_st_Re_experience.csv'); %read literature data
% plot(LiteratureData(:,1),LiteratureData(:,2),'ko','LineWidth',2);xlabel('Re');ylabel('St');
% box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
% set(gca,'FontSize', 18);
% legend('Linear','WNL','HB1','Ref. [23]','Location','northwest');
% saveas(gca,'FIGURES/Cylinder_Strouhal_Re_HB',figureformat);
% 
% figure(22);hold off;
% plot(Re_LIN,Fx_LIN,'b+-');
% hold on;
% plot(Re_WNL,Fx_WNL,'g--','LineWidth',2);hold on;
% plot(Re_HB,Fx_HB,'r+-','LineWidth',2);
% plot(Rec,Fxc,'ro')
% xlabel('Re');ylabel('Fx');
% box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
% set(gca,'FontSize', 18);
% legend('BF','WNL','HB1','Location','south');
% saveas(gca,'FIGURES/Cylinder_Cx_Re_HB',figureformat);
% 
% figure(23);hold off;
% plot(Re_LIN,Lx_LIN,'b+-');
% hold on;
% plot(Re_HB,Lx_HB,'r+-','LineWidth',2);
% LiteratureData=csvread('./literature_data/fig7e_Lx_mean.csv'); %read literature data
% plot(LiteratureData(:,1),LiteratureData(:,2),'ko','LineWidth',2);
% plot(Rec,Lxc,'ro','LineWidth',2);
% xlabel('Re');ylabel('Lx');
% box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
% set(gca,'FontSize', 18);
% legend('BF','HB1','Ref. [5]','Location','northwest');
% saveas(gca,'FIGURES/Cylinder_Lx_Re_HB',figureformat);
% 
% figure(24);hold off;
% plot(Re_WNL,abs(Fy_WNL),'g--','LineWidth',2);
% hold on;
% plot(Re_HB,real(Fy_HB),'r+-','LineWidth',2);
% %title('Harmonic Balance results');
% xlabel('Re');  ylabel('Fy')
% box on;  pos = get(gcf,'Position');  pos(4)=pos(3)*AspectRatio;  set(gcf,'Position',pos); % resize aspect ratio
% set(gca,'FontSize', 18);
% legend('WNL','HB1','Location','south');
% saveas(gca,'FIGURES/Cylinder_Cy_Re_SC',figureformat);
% 
% figure(25);hold off;
% plot(Re_WNL,A_WNL,'g--','LineWidth',2);
% hold on;
% plot(Re_HB,Aenergy_HB,'r+-','LineWidth',2);
% LiteratureData=csvread('./literature_data/fig7d_energy_amplitude.csv'); %read literature data
% plot(LiteratureData(:,1),LiteratureData(:,2),'ko','LineWidth',2);
% %title('Harmonic Balance results');
% xlabel('Re');ylabel('A_E')
% box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio; set(gcf,'Position',pos); % resize aspect ratio
% set(gca,'FontSize', 18);
% legend('WNL','HB1','Ref. [5]','Location','south');
% if(meshstrategy=='D')
%     filename = 'FIGURES/Cylinder_Energy_Re_SC_AdaptD';
% else
%     filename = 'FIGURES/Cylinder_Energy_Re_SC_AdaptS';
% end
% saveas(gca,filename,figureformat);
% 
% 
% tnolin = toc;
% disp(' ');
% disp('       cpu time for Nonlinear calculations : ');
% disp([ '   ' num2str(tnolin) ' seconds']);
% 
% disp(' ');
% disp('Total cpu time for the linear & nonlinear calculations and generation of all figures : ');
% disp([ '   ' num2str(tlin+tnolin) ' seconds']);
% 
% 
% save('Results_Cylinder_NONLINEAR.mat');


