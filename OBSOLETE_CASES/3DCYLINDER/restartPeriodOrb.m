%%  NONLINEAR STABILITY ANALYSIS of the wake of a rotating of a cylinder with STABFEM  
%

%% CHAPTER 4 : computation of weakly nonlinear expansion

disp(' ');
disp('######     ENTERING NONLINEAR PART       ####### ');
disp(' ');
close all;
addpath('../../SOURCES_MATLAB/');
verbosity=4;ffdatadir='./WORK/';
SF_Start; 

ffmesh = SFcore_ImportMesh('./WORK/MESHES/mesh_Init.msh','problemtype','2D');
ffmesh.symmetry='N';
Re_HB = [85:1:200];
alpha_HB = linspace(5.29,4.35,length(Re_HB));

%[meanflow,mode] = SF_HB1(meanflow,mode,'sigma',0.,'Re',Res); 
%meanflow = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/MeanFlow_Re83_Omegax5.2984.ff2m');
%mode = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/Harmonic1_Re83_Omegax5.2984.ff2m');
%mode2 = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/Harmonic2_Re83_Omegax5.2984.ff2m');

meanflow = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/MeanFlow_Re170_Omegax4.5069.ff2m');
mode = SFcore_ImportData(ffmesh,'./WORK/MEANFLOWS/HBMode1_Re170_Omegax4.5069.ff2m');
meanflow.datatype = 'Meanflow';
[meanflow,mode] = SF_Adapt(meanflow,mode,'Hmax',1);
alpha_List = [4.50:-0.01:4.2];

%% Loop to inspect Hopf bif
for i = [1:length(alpha_List)]
	alpha = alpha_List(i);
	[meanflow,mode] = SF_HB1(meanflow,mode,'Re',170,'Omegax',...
                             alpha,'symmetry','N','symmetryBF','N');
end	

%% Loop until Re=200
%for i = [1:length(Re_HB)]
%    Re = Re_HB(i);
%    alpha = alpha_HB(i);
%    [meanflow,mode] = SF_HB1(meanflow,mode,'Re',Re,'Omegax',...
%                             alpha,'symmetry','N','symmetryBF','N');
    %[meanflow,mode,mode2] = SF_HB2(meanflow,mode,mode2,'Re',Re,'Omegax',...
    %                         alpha,'symmetry','N','symmetryBF','N');  
%end

%%

