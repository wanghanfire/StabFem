  
% THIS SCRIPT DEMONSTRATES HOW TO DO DNS with StabFem

%%

run('../../SOURCES_MATLAB/SF_Start.m');verbosity = 20;
figureformat='png'; AspectRatio = 0.56; % for figures
% If the mesh is not created yet, please create it here
alpha = 5.25;
ffmesh = SFcore_ImportMesh('./WORK/mesh.msh','problemtype','2d');
[ev,em] = SF_Stability(bf,'shift',0.2,'nev',5,'type','D');
[ev3D,em3D] = SF_Stability(bf,'k',0.0001,'shift',0.2,'nev',5,'type','D');

% Re = 200.008 unstable / Re = 200.004 Saddle / Re = 200.001 Stable
%% Chapter 2 : Launch a DNS

%  We do 30 000 time steps and produce output each 100 timestep 
bf=SF_BaseFlow(ffmesh,'Re',200,'Omegax',5.25);
startfield = SF_Add(bf,bf,'Amp2',0); % creates startfield = bf+0.01*em

CFL = 0.25;
Nit = 100000; iout = 100;dt = 0.00025;
[DNSstats,DNSfields] =SF_DNS(startfield,'Re',200.,'Nit',Nit,'dt',dt,'iout',iout,'Omegax',6)

% Nit = 1000; iout = 10;dt = 0.002;
% [DNSstats,DNSfields] =SF_DNS(DNSfields(end),'Re',200,'Nit',Nit,'dt',dt,'iout',iout)

% this one is for postprocessing only, without launching everything again

% i=1;
% h = figure;
% filename = 'html/DNS_Cylinder_Re60.gif';
% SF_Plot(DNSfields(i),'ux','xlim',[-2 10],'ylim',[-3 3 ],'title',['t = ',num2str(i-1)*dt],'boundary','on')
% set(gca,'nextplot','replacechildren');
% 
% %% Generate a movie
% 
% for i=1:20:Nit/iout
%     SF_Plot(DNSfields(i),'ux','xlim',[-2 10],'ylim',[-3 3 ],'title',['t = ',num2str(i)],'boundary','on');
%     pause(0.1);
% end


%%
% Here is the movie
%
% <<DNS_Cylinder_Re60.gif>>
%

%% Plot the lift force as function of time

figure(15);
subplot(2,1,1);
plot(DNSstats.tps,DNSstats.Lift);
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstats.tps,DNSstats.Drag);
xlabel('t');ylabel('Fx');

figure(16);
subplot(2,1,1);
plot(DNSstatsL.Drag(:),DNSstatsL.Lift(:));
xlabel('Fx');ylabel('Fy');
subplot(2,1,2);
plot(DNSstatsOld.Drag(10:end),DNSstatsOld.Lift(10:end));
xlabel('Fx');ylabel('Fy');




%% Chapter 3 : how to do a DNS starting from a previous snapshot
%  We do another 1000 time steps starting from previous point 

%DNSfield = DNSfields(end);
%[DNSstats2,DNSfields] = SF_DNS(DNSfield,'Re',60,'Nit',20,'dt',0.005,'iout',100)

