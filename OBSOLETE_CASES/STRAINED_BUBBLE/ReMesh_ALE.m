
function bf = ReMesh_ALE(bf)
Oh = bf.Oh;We =bf.We;
Rmax = max(bf.mesh.rsurf); Zmax = max(bf.mesh.zsurf);
Mask = SF_Mask(bf,[0 Rmax+.2 0 Zmax+.2 .025]);
%Mask = SF_Launch('Heat_Unsteady.edp','Params',300,'Mesh',bf.mesh,'DataFile','Heat_unsteady.ff2m');
if We>0 
    Mask2 = SF_Mask(bf,[0 Rmax+1 0 Zmax+4 .2]);
else
    Mask2 = SF_Mask(bf,[0 Rmax+4 0 Zmax+1 .2]);
end
bf = SF_Adapt(bf,Mask,Mask2,'Hmax',.5,'Ratio',1);
bf = SF_Deform(bf,'Oh',Oh,'dS',1e-6);

end