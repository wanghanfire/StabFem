
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB']);
SF_Start('verbosity',4);
close all;

Oh = 1e-4;
bf = SF_Init('Mesh_square_light.edp','problemtype','strainedbubble');
bf = SF_Deform(bf,'Oh',1e-2,'We',0.01);% first step
bf = SF_Deform(bf,'Oh',Oh,'We',0.01);% first step


 ev1a = SF_Stability(bf,'nev',10,'shift',2+2i,'m',1,'sym','S','sort','LR','plotspectrum',true);
 ev1b = SF_Stability(bf,'nev',10,'shift',2+2i,'m',1,'sym','S','sort','LR','plotspectrum',true,'solver','Stab_Axi_ALE_StrainedBubble.edp');
% code works but results have a problem.
 ev0a = SF_Stability(bf,'nev',10,'shift',2+2i,'m',0,'sym','S','sort','LR','plotspectrum',true);
 ev0b = SF_Stability(bf,'nev',10,'shift',2+2i,'m',0,'sym','S','sort','LR','plotspectrum',true,'solver','Stab_Axi_ALE_StrainedBubble.edp');  

 %%% TESTS A REPRENDRE !
 %% Verifier Surf_interp4 et Surf_interp4_Paul
 %  diff ../../SOURCES_FREEFEM/INCLUDE/Macros_Paul.idp ../../SOURCES_FREEFEM_DEVELOP/INCLUDE/Macros_Paul.idp 