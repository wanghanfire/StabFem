addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB']);
SF_Start('workdir','2axial_01','verbosity',4);
close all;
SF_core_arborescence('cleanall')

Oh = 0.1;symb = 'xr';




%%

% Generate a mesh and a starting flow
%bf = SF_Init('Mesh_square_20.edp','problemtype','axifreesurf');
bf = SF_Init('Mesh_square_20.edp','problemtype','strainedbubble');

% Plot the shape
RR = [ bf.mesh.rsurf ;  flip(bf.mesh.rsurf) ; -bf.mesh.rsurf  ; -flip(bf.mesh.rsurf)];
ZZ = [ bf.mesh.zsurf ; -flip(bf.mesh.zsurf) ; -bf.mesh.zsurf  ;  flip(bf.mesh.zsurf)] ;
figure(20); plot(RR,ZZ);hold on;axis equal;

% Aspect ratio and statistics
chi0(1) = 1; chiM(1) = 1;chim(1)=1;
Wetab(1) = 0;
Pbtab(1) = 2;
figure(13); plot(Wetab(1),Pbtab(1),symb); hold on;xlabel('We');ylabel('Pb');
hold on;xlabel('We');ylabel('Pb');

figure(14); plot(Wetab(1),chi0(1),'+',Wetab(1),chiM(1),'x',Wetab(1),chim(1),'*'); 
hold on;xlabel('We');ylabel('\chi,\chi_M,\chi_m');

%% 
% Deform this Mesh+baseflow for specified We and Oh.
% 

bf = SF_Deform(bf,'Oh',0.1,'We',-0.025);% first step
bf = SF_Deform(bf,'Oh',Oh,'We',-0.1);
RR = [ bf.mesh.rsurf ; flip(bf.mesh.rsurf)  ; -bf.mesh.rsurf ; -flip(bf.mesh.rsurf)];
ZZ = [ bf.mesh.zsurf ; -flip(bf.mesh.zsurf) ; -bf.mesh.zsurf  ; flip(bf.mesh.zsurf)];
figure(20);plot(RR,ZZ);hold on;xlabel('R'),ylabel('Z');

% Aspect ratio and statistics
Wetab(2) = bf.We;
Pbtab(2) = bf.Pb;
Z0 = bf.mesh.zsurf(1); ZM = max(bf.mesh.zsurf);R0 = bf.mesh.rsurf(end);RM = max(bf.mesh.rsurf);
chi0(2) = Z0/R0;chiM(2) = Z0/RM;chim(2) = ZM/R0;
figure(13); plot(abs(Wetab(2)),Pbtab(2),symb); hold on;
figure(14); plot(abs(Wetab(2)),chi0(2),'+',abs(Wetab(2)),chiM(2),'x',abs(Wetab(2)),chim(2),'*'); 

%%
% plot this flow
%
psilevels = sqrt(abs(bf.We))*[-2 -1 -0.5 -0.2 -0.1 -0.05 -0.01 0 0.01 0.05];
figure(100);  SF_Plot(bf,'vort','xlim',[0 3],'ylim',[0 3])
hold on; SF_Plot(bf,'psi','contour','only','xlim',[0 3],'ylim',[0 3],'clevels',psilevels);


%%
% Then we may try to adapt the mesh...
bf = ReMesh_ALE(bf);
figure(101);SF_Plot(bf,'mesh','title','Adapted mesh (experiment)')

%%
% Computes eigenvalues
%[ev,em] = SF_Stability(bf,'nev',10,'shift',3+1i,'sym','A');



%%
% Start a loop in continuation mode
dSmax = 0.1;
dS = dSmax;
for i = 3:35
    
  %  compute next step (possibly reduce the step) 
  bf = SF_Deform(bf,'Oh',Oh,'dS',dS);
  if bf.iter==-1
    dS = dS/2;
    bf = SF_Deform(bf,'Oh',Oh,'dS',dS);
    if bf.iter==-1
      dS = dS/5;
      bf = SF_Deform(bf,'Oh',Oh,'dS',dS);
      if bf.iter==-1
        error('Severe divergence in newton loop');
        i = i-1;
        break;
      end
    end
  end
  dS = min(dSmax,dS*1.1); % try increasing again for next step
  figure(13);plot(abs(bf.We),bf.Pb,'.');

  % Remesh 
  bf = ReMesh_ALE(bf);
  
  % Extract and and plot the shape
  RR = [ bf.mesh.rsurf ; flip(bf.mesh.rsurf)  ; -bf.mesh.rsurf ; -flip(bf.mesh.rsurf)];
  ZZ = [ bf.mesh.zsurf ; -flip(bf.mesh.zsurf) ; -bf.mesh.zsurf  ; flip(bf.mesh.zsurf)] ;
  figure(20);plot(RR,ZZ);
 
  % aspect ratios and statistics
  Wetab(i) = bf.We;
  Pbtab(i) = bf.Pb;
  Z0 = bf.mesh.zsurf(1); ZM = max(bf.mesh.zsurf);R0 = bf.mesh.rsurf(end);RM = max(bf.mesh.rsurf);
  chi0(i) = Z0/R0;chiM(i) = Z0/RM;chim(i) = ZM/R0;
  figure(13); plot(abs(Wetab(i)),Pbtab(i),symb); hold on;
  figure(14); plot(abs(Wetab(i)),chi0(i),'+',abs(Wetab(i)),chiM(i),'x',abs(Wetab(i)),chim(i),'*'); 
  
  % Stability (here m=2,S)
   ev2S = SF_Stability(bf,'nev',10,'shift',2+2i,'m',2,'sym','S','sort','LR');
   ev2Smaxtab(i) = ev2S(1);  
    figure(15);
      subplot(2,1,1);xlabel('We'),ylabel('\lambda_r');hold on;
        plot(Wetab(i)*ones(size(ev2S)),real(ev2S),'+b');
      subplot(2,1,2);xlabel('We'),ylabel('\lambda_i');hold on;
        plot(Wetab(i)*ones(size(ev2S)),imag(ev2S),'+b');
  
   % Stability (here m=0,S)
%   ev0S = SF_Stability(bf,'nev',10,'shift',2+2i,'m',0,'sym','S','sort','LR');
%   ev0Smaxtab(i) = ev2S(1);  
%    figure(15);
%      subplot(2,1,1);xlabel('We'),ylabel('\lambda_r');hold on;
%        plot(Wetab(i)*ones(size(ev0S)),real(ev2S),'or');
%      subplot(2,1,2);xlabel('We'),ylabel('\lambda_i');hold on;
%        plot(Wetab(i)*ones(size(ev0S)),imag(ev2S),'or');      
        
  pause(0.01);

end


%% Plot final figures

figure(23); plot(abs(Wetab(1:i)),Pbtab(1:i));
xlabel('We');ylabel('Pb');

figure(24); plot(abs(Wetab(1:i)),chiM(1:i),'--k',abs(Wetab(1:i)),chim(1:i),'-.k',abs(Wetab(1:i)),chi0(1:i),'-k'); 
xlabel('We');ylabel('\chi,\chi_M,\chi_m');
 
 figure(25);
      subplot(2,1,1);xlabel('We'),ylabel('\lambda_r');hold on;
      plot(Wetab(1:i),real(ev2Smaxtab(1:i)),'-b');
      subplot(2,1,2);xlabel('We'),ylabel('\lambda_i');hold on;
      plot(Wetab(1:i),real(ev2Smaxtab(1:i)),'-b');
pause;

%%
% Stability calculations for m=0, m=1, m=2

%sfstatus = SF_Status('ALL');
%close(34); close(35);
%for i=1:length(sfstatus.MESHES)
% % for i=131:136
%    bf = SF_Load('mesh',i);    
%    Wetab(i) = bf.We;
%    Pbtab(i) = bf.Pb;
%    if i==1
%        Stab(i) = 0;
%    else
%        sqrt((Wetab(i)-Wetab(i-1))^2+(Pbtab(i)-Pbtab(i-1))^2);
%        Stab(i) = Stab(i-1)+sqrt((Wetab(i)-Wetab(i-1))^2+(Pbtab(i)-Pbtab(i-1))^2);
%    end

%      ev0A = SF_Stability(bf,'nev',10,'shift',3+1i,'m',0,'sym','A');
%      ev0S = SF_Stability(bf,'nev',10,'shift',3+1i,'m',0,'sym','S');
%      ev1A = SF_Stability(bf,'nev',10,'shift',3+1i,'m',1,'sym','A');
%      ev1S = SF_Stability(bf,'nev',10,'shift',3+1i,'m',1,'sym','S');
%      ev2A = SF_Stability(bf,'nev',10,'shift',3+1i,'m',2,'sym','A');
%      ev2S = SF_Stability(bf,'nev',10,'shift',10+1i,'m',2,'sym','S');
      


%      figure(34);
%      subplot(2,1,1);xlabel('S'),ylabel('\lambda_r');hold on;
%      plot(Stab(i)*ones(size(ev0A)),real(ev0A),'or');plot(Stab(i)*ones(size(ev0S)),real(ev0S),'ob');
%      plot(Stab(i)*ones(size(ev1A)),real(ev1A),'xr');plot(Stab(i)*ones(size(ev1S)),real(ev1S),'xb');
      %plot(Stab(i)*ones(size(ev0A)),real(ev2A),'+r');
%      plot(Stab(i)*ones(size(ev2S)),real(ev1S),'+b');
%      subplot(2,1,2);xlabel('S'),ylabel('\lambda_i');hold on;
%      plot(Stab(i)*ones(size(ev0A)),imag(ev0A),'or');plot(Stab(i)*ones(size(ev0S)),imag(ev0S),'ob');
%      plot(Stab(i)*ones(size(ev1A)),imag(ev1A),'xr');plot(Stab(i)*ones(size(ev1S)),imag(ev1S),'xb');
      %plot(Stab(i)*ones(size(ev0A)),imag(ev2S),'+r');
%      plot(Stab(i)*ones(size(ev2S)),imag(ev1S),'+b');

%     figure(35);
%      subplot(2,1,1);xlabel('We'),ylabel('\lambda_r');hold on;
%      plot(Wetab(i)*ones(size(ev0A)),real(ev0A),'or');plot(Wetab(i)*ones(size(ev0S)),real(ev0S),'ob');
%      plot(Wetab(i)*ones(size(ev1A)),real(ev1A),'xr');plot(Wetab(i)*ones(size(ev1S)),real(ev1S),'xb');
%      plot(Wetab(i)*ones(size(ev0A)),real(ev2A),'+r');
%plot(Wetab(i)*ones(size(ev2S)),real(ev2S),'+b');
%      subplot(2,1,2);xlabel('We'),ylabel('\lambda_i');hold on;
%      plot(Wetab(i)*ones(size(ev0A)),imag(ev0A),'or');plot(Wetab(i)*ones(size(ev0S)),imag(ev0S),'ob');
%      plot(Wetab(i)*ones(size(ev1A)),imag(ev1A),'xr');plot(Wetab(i)*ones(size(ev1S)),imag(ev1S),'xb');
%      plot(Wetab(i)*ones(size(ev0A)),imag(ev2S),'+r');
%plot(Wetab(i)*ones(size(ev2S)),imag(ev2S),'+b');

%pause(0.01);

%end

