SF_core_start('workdir','2axial_01_BAK_Mesh10','verbosity',3);
Oh = 0.1;symb = 'xr';


sfstatus = SF_Status('ALL');
%for i=1:length(sfstatus.MESHES)
for i=1:62
    bf = SF_Load('mesh',i);    
    Wetab(i) = bf.We;
    Pbtab(i) = bf.Pb;
    
    % Extract and and plot the shape
  RR = [ bf.mesh.rsurf ; flip(bf.mesh.rsurf)  ; -bf.mesh.rsurf ; -flip(bf.mesh.rsurf)];
  ZZ = [ bf.mesh.zsurf ; -flip(bf.mesh.zsurf) ; -bf.mesh.zsurf  ; flip(bf.mesh.zsurf)] ;
  figure(20);plot(RR,ZZ);hold on;
 
  % aspect ratios and statistics
  Wetab(i) = bf.We;
  Pbtab(i) = bf.Pb;
  Z0 = bf.mesh.zsurf(1); ZM = max(bf.mesh.zsurf);R0 = bf.mesh.rsurf(end);RM = max(bf.mesh.rsurf);
  chi0(i) = Z0/R0;  chiM(i) = Z0/RM;  chim(i) = ZM/R0;
   chi0(i)
   chiM(i)
   chim(i)
  figure(13); plot(abs(Wetab(i)),Pbtab(i),symb); hold on;
  figure(14); plot(abs(Wetab(i)),chi0(i),'+',abs(Wetab(i)),chiM(i),'x',abs(Wetab(i)),chim(i),'*');hold on; 
  pause(0.1);
end

%% Plot final figures

figure(23); plot(abs(Wetab(1:i)),Pbtab(1:i));
xlabel('We');ylabel('Pb');

figure(24); plot(abs(Wetab(1:i)),chiM(1:i),'--k',abs(Wetab(1:i)),chim(1:i),'-.k',abs(Wetab(1:i)),chi0(1:i),'-k'); 
xlabel('We');ylabel('\chi,\chi_M,\chi_m');
 
lambdaM = sfs.EIGENVALUES.lambda(real(sfs.EIGENVALUES.lambda)>-.1)
WeM = sfs.EIGENVALUES.We(real(sfs.EIGENVALUES.lambda)>-.1)

figure(25);
      subplot(2,1,1);xlabel('We'),ylabel('\lambda_r');hold on;
      plot(WeM,real(lambdaM),'-b');
      subplot(2,1,2);xlabel('We'),ylabel('\lambda_i');hold on;
      plot(WeM,imag(lambdaM),'-b');
      
      
