addpath('../../SOURCES_MATLAB/')
SF_Start('verbosity',4);

% generating mesh
r0 = 0.01;
L = 1;
N=5; % mesh density on the collector and emitter
nb=(N*5)^2; %number of elements in mesh
ffmesh = SF_Mesh('Mesh_cyl.edp','Params',[r0, L, N, nb],'problemtype','Kaptsov');

% solving problem 
bf = SF_BaseFlow(ffmesh,'Eac',1);

%solution exacte
J=-1.020509484;
K=-2.0509484*10^(-6);
J=-J;
phi_th=@(r) ((J+K)^0.5-(J*r.^2+K).^0.5+K^0.5*log(((J*r.^2+K).^0.5+K^0.5)./(r*((J+K)^0.5+K^0.5))));
n_th=@(r)(J./sqrt(K+J*r.^2));
% %plots
% figure; SF_Plot(bf,'phi');
rline = [r0:0.0005:L];
phi_thn=phi_th(rline);
n_thn=n_th(rline);
% philine = SF_ExtractData(bf,'phi',rline,0)
% figure; plot(rline,philine,'r',rline,phi_thn,'b');
% diff1=max(abs(philine-phi_thn));
% % adapting mesh
% bf = SF_Adapt(bf,'Hmax',.1);
% figure; SF_Plot(bf,'phi');
% figure; SF_Plot(bf,'n');
% philine = SF_ExtractData(bf,'phi',rline,0)
% figure; plot(rline,philine,'r',rline,phi_thn,'b');
% diff2=max(abs(philine-phi_thn));

for i =1:9
    N=5+i*10; %number of elements in mesh
    if i==1
        nb=(N)^2;
    else
        nb=(N)^2;
    end
ffmesh = SF_Mesh('Mesh_cyl.edp','Params',[r0, L, N, nb],'problemtype','Kaptsov');% solving problem 
figure(); SF_Plot(bf,'mesh')
bf = SF_BaseFlow(ffmesh,'Eac',1);
philine = SF_ExtractData(bf,'phi',rline,0);
nline=SF_ExtractData(bf,'n',rline,0);
diff1(i)=max(abs(philine-phi_thn));
diffn1(i)=max(abs(nline-n_thn));
% normeL2n(i)=2*pi*trapz(rline,n_thn.*n_thn.*rline);
% normeL2phi(i)=2*pi*trapz(rline,phi_thn.*phi_thn.*rline);
% normeL2_n_sym(i)=bf.L2n;
% normeL2_phi_sym(i)=bf.L2phi;
% normeL2_produit_phi=2*pi*trapz(rline,2*phi_thn.*philine.*rline);
% normeL2_produit_n=2*pi*trapz(rline,2*n_thn.*nline.*rline);
% diff_L2_phi(i)=normeL2phi(i)+normeL2_phi_sym(i)-normeL2_produit_phi;
% diff_L2_n(i)=normeL2n(i)+normeL2_n_sym(i)-normeL2_produit_n;
diff_L2_phi(i)=bf.L2phi;
diff_L2_n(i)=bf.L2n;
bf = SF_Adapt(bf,'Hmax',0.05);
philine = SF_ExtractData(bf,'phi',rline,0);
nline=SF_ExtractData(bf,'n',rline,0);
diff2(i)=max(abs(philine-phi_thn));
diffn2(i)=max(abs(philine-phi_thn));
end
figure();loglog(mesh,diff1);
title('phi avant adapt');
% figure();plot(diff2);
% title('phi apres adapt');
figure();loglog(mesh,diffn1);
title('n avant adapt');
% figure();plot(diffn2);
% title('n apres adapt');
figure();loglog(mesh,diff_L2_n);
title('diff norme L2 n avant adapt');
figure();loglog(mesh,diff_L2_phi);
title('diff norme L2 phi avant adapt');