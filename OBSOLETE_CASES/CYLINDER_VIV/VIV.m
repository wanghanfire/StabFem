%%  LINEAR Stability ANALYSIS of the wake of a cylinder with STABFEM  
%
% 
%  this scripts demonstrates how to use StabFem to study the instabilities
%  in a Vortex-Induced Vibrations (VIV) problem. 
%
%  The script reproduces figures from the paper by D. Sabino et al.
%  
%  The script performs the following calculations :
% 
% 
%  # Generation of an adapted mesh
%  # Impedance computations for Re = 35 (figure 2c and 3a)  
%  # Stability curves St(U*) and sigma(U*) (figure 4a) 
%
%%
%
% First we set a few global variables needed by StabFem
%

close all;
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./WORK/'); 
figureformat='png'; 
system('mkdir FIGURES');

sfstat = SF_Status();
if strcmp(sfstat.status,'new')
    bf = SmartMesh_Cylinder; % or your own sequence for mesh/bf generation
else
    bf = SF_Load('lastadapted');  
end
%bf.mesh.problemtype = '2Dmobile'; % to be fixed in a better way


%% Eigenvalue computation
bf.mesh.problemtype = '2DMobile';
bf = SF_BaseFlow(bf,'Re',60);

mstar = 20; Ustar = 3;
M = mstar*pi/4; K = pi^3*mstar/Ustar^2;

% mode "FM"
shiftFM = 0.04689 + 0.74874i; 
[evFM,emFM] = SF_Stability(bf,'shift',shiftFM,'nev',10,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0,'plotspectrum','yes');
evFM(1)
figure;SF_Plot(emFM(1),'vort','xlim',[-2 4],'ylim',[0 3]);

% mode "EM"
shiftEM = -0.0188 + 2.0100i; 
[evEM,emEM] = SF_Stability(bf,'shift',shiftEM,'nev',10,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0,'plotspectrum','yes');
evEM(1)
figure;SF_Plot(emEM(1),'vort','xlim',[-2 4],'ylim',[0 3]);

% computing branch "FM" for a range of Ustar

Ustar_tab = [3:.1:11];
K = pi^3*mstar/Ustar_range(1)^2;
ev = SF_Stability(bf,'shift',shiftFM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);

for k = 1:length(Ustar_tab)
  Ustar = Ustar_tab(k);
  K = pi^3*mstar/Ustar^2;
  ev = SF_Stability(bf,'shift','cont','nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);
  FM_tab(k) = ev;
end


% computing branch "EM" for a range of Ustar

Ustar_tab = [3:.1:11];
K = pi^3*mstar/Ustar_range(1)^2;
ev = SF_Stability(bf,'shift',shiftEM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);

for k = 1:length(Ustar_tab)
  Ustar = Ustar_tab(k);
  K = pi^3*mstar/Ustar^2;
  ev = SF_Stability(bf,'shift','cont','nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);
  EM_tab(k) = ev;
end

% plotting both these branches

figure(35);
subplot(2,1,1);
plot(Ustar_tab,real(FM_tab),'b');hold on;
plot(Ustar_tab,real(EM_tab),'r');
xlabel('U*'); ylabel('\lambda_r');
legend('FM','EM');
subplot(2,1,2);
plot(Ustar_tab,imag(FM_tab)/(2*pi),'r');hold on;
plot(Ustar_tab,imag(EM_tab)/(2*pi),'r');
xlabel('U*'); ylabel('St');
legend('FM','EM');

%% Impedance computation

bf = SF_BaseFlow(bf,'Re',35);
figure;
fo = SF_LinearForced(bf,'omega',[0:.01:1],'plot','yes');
