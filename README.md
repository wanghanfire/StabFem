# StabFem

## *Latest news (26/06/2020)*

* New stable version 3.5 compatible with Mac/Linux/Windows10 and Octave/Matlab is released !

* A new website allowing automatic publication of programs is here : https://stabfem.gitlab.io/StabFem/newwebsite/

* The showcase website of the project, with several examples and tuturials, is 
[here](https://stabfem.gitlab.io/StabFem/).

* A reference manual (beta version) is available [here](https://gitlab.com/stabfem/StabFem/blob/develop/99_Documentation/MANUAL/main.pdf).


A presentation of the main functionalities of the StabFem project was presented last year
(FreeFem++ days, dec. 13, 2018). The main document of the presentation is available
[here](https://gitlab.com/stabfem/StabFem/blob/master/99_Documentation/PRESENTATIONS/Beamer_13dec2018_handout.pdf).

The presentation is also based on three commented programs : [Basic example](https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape.pdf),
[acoustic pipes](https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/ACOUSTICS_PIPES/html/Acoustic%20field%20in%20a%20pipe%20with%20harmonic%20forcing%20at%20the%20bottom.pdf),
and [DNS cylinder](https://gitlab.com/stabfem/StabFem/blob/master/DEVELOPMENT_CASES/CYLINDER_DNS/html/SCRIPT_DNS_EXAMPLE.pdf).



## General description of the project

StabFem is a set of programs to perform Global Stability calculations in Fluid Mechanics, which is developed 
for both research and education purposes.

The project is multi-system (linux, macOS, Windows), and based on two softwares :

- The finite-element software FreeFem++ is used to generate the meshes, construct the operators
and solve the various linear problems involved in the computation.

- Matlab/Octave is used as a driver to monitor the computations in terminal or script mode and as a graphical interface to plot the results.

The classes of problems currently integrated are as follows:
- Incompressible flows (wakes and jets, flow through conduits, etc...)
- Compressible flows,
- Fluid-structure interactions for rigid bodies (spring-mounted objects or freely falling objects),
- Boussinesq flows (stratified flows, Rayleigh-Taylor convection, ...)
- Static free-surface problems (sloshing problems, liquid bridges, etc..)
- Free-surface flows with deformable surfaces using Arbitrary Lagrangian Eulerian (ALE) framework,
- Linear acoustics,
- (...)

The kind of computations currently implemented comprises :
- Computation of steady equilibrium solutions (base flow) using  Newton iteration, including arclength continuation,
- Computation of eigenvalue/eigenmodes, including interactive exploration of the spectrum, adjoint and stuctural sensitivity approaches, 
- Direct Numerical simulation (DNS)
- Computation of nonlinear limit cycles using Harmonic Balance approach, 
- Powerful and versatile mesh adaptation, 
- (...)

Currently, 3D flows can be handled, but geometries have to be 2D or axisymmetric.


## Example

Suppose you have three FreeFem++ programs Mesh.edp, Newton_2D.edp and Stab_2D.edp to define a mesh, compute a steady flow trough Newton iteration and compute eigenvalues.


Here is the sequence of commands you should type in a Matlab terminal (or in a Matlab script)
to run these programs through the relevant drivers, and directly plot the results.


```
SF_Start(); % Stabfem initialization
ffmesh = SF_Mesh('Mesh.edp'); % constructs and imports a mesh from a freefem program
bf= SF_Baseflow(ffmesh,'Re',1); % constructs a "guess" base flow for a low Re 
bf = SF_BaseFlow(bf,'solver','Newton.edp',Re',100) % computes a Baseflow for Re = 100
SF_Plot(bf,'p');  % plot the pressure with color levels
hold on; SF_Plot('psi','contour','only')
bf = Sf_Adapt(bf);      % adapts the mesh
shift=-0.1549 + 5.343i;
[ev,em] = SF_Stability(bf,'solver','Stab_2d.edp',shift,'nev',1) % computes one eigenmode
SF_Plot(em,’ux.re’) % plots the real part of the axial velocity component of the eigenmode
```

## How to install and use this software ?

### Requiremements

Before installation you need to have :

- Linux (Ubuntu 18 or other), MacOS (10.0 or earlier), Windows 10 (for other systems please contact the developers)


- [Octave](https://www.gnu.org/software/octave/Octave) (4.4.0 or later)  or [Matlab](https://www.mathworks.com/products/matlab.html) (2017b or later)

- FreeFem++ (version 3.61 or later).

For students and casual users it is advised to downoad one of the precompiled versions of FreeFem available 
[here](http://www3.freefem.org).

For advanced users and developers it is advised to compile FreeFem from the full sources available [here](https://github.com/FreeFem/FreeFem-sources/releases).

- If using windows 10 you may additionnally need to download  [git](https://gitforwindows.org).



### StabFem Installation

- For students and casual users it is advised to simply download a single branch by clicking the "download" button.

- For advanced users it is advised to clone the full project by typing the following command in a shell or git terminal:

```
git clone https://gitlab.com/stabfem/StabFem
```

Once you have downloaded everything, launch matlab (or Octave), open any of the scripts of the project and simply run it !

It is advised to start with one of the following tutorial scripts :


- [STABLE_CASES/CYLINDER/CYLINDER_LINEAR.m](https://stabfem.gitlab.io/StabFem//CYLINDER/CYLINDER_LINEAR.html) for a simple example of using StabFem as a Matlab/Octave interface for 
FreeFem++.


- [STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape.m](https://stabfem.gitlab.io/StabFem/stable/lshape/SCRIPT_Lshape.html) for a simple example of using StabFem for linear stability analysis of a wake flow.

In case of problems you may check the file [Instalationnotes.md](https://gitlab.com/stabfem/StabFem/blob/master/InstalationNotes.md) 

## Getting started ?

The StabFem project contains a large number of tutorial examples, either on the showcase 
[website](https://stabfem.gitlab.io/StabFem/) or in the 
[refecence manual](https://gitlab.com/stabfem/StabFem/blob/develop/99_Documentation/MANUAL/main.pdf). Different starting points can be recommended depending upon your needs :

1. If you are interested in using StabFem for global stability analyses of a class of problem already implemented in the project, we recommend that you first study the[STABLE_CASES/CYLINDER/CYLINDER_LINEAR.m](https://stabfem.gitlab.io/StabFem//CYLINDER/CYLINDER_LINEAR.html)
tutorial example, along with the [AMR (2019)](https://gitlab.com/stabfem/StabFem/blob/master/99_Documentation/ARTICLE_STABFEM/ARTICLE_ASME_Accepted.pdf)
 research paper. Then you may look at section 3.5 of the manual to see how to customize to your case of interest.
 

2. If you are interested in using StabFem as a Matlab/Octave interface to FreeFem, but not specifically for flow instability studies, we recommend that you study the tutorial example 
[STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape.m](https://stabfem.gitlab.io/StabFem/stable/lshape/SCRIPT_Lshape.html) along with chapter 2 of the manual.

3. If you are already familiar with FreeFem and want to incorporate your own codes into the project, it is recommended that you study chapters 2 and 3 of the manual.

4. If you do not have/like the Matlab/Octave interface and are only interested in the FreeFem++ part of the project, good news ! you can perfectly use the FreeFem programs  outside of the interface (for instance in a shell terminal or script). Section 3.6 of the manual will explain how to do this.


## Test-cases currently implemented :

- CYLINDER : 

Study of the Bénard-Von Karman instability for Re>47 in the wake of a cylinder. This directory contains scripts performing the base flow computation, linear stability analysis, adjoint and sensitivity analysis, as well as a nonlinear determination of the limit cycle using "harmonic balance" method.

- DISK_IN_PIPE :

An example in axisymmetric coordinates : flow around a disk within a pipe. This directory contains a demonstrator of the software which performs the stability analysis, displays the spectrum, allows to click on eigenvalues to plot the corresponding eigenmodes, computes instability branches (eigenvalue as function of Re), and performs the weakly nonlinear analysis of the leading instability (steady, non-axisymmetric).

- LiquidBridge :

An example with a free surface : computation of the equilibrium shape of a liquid bridge and of the oscillation modes of this bridge (in the inviscid, potential case).

(see Chireux et al., Phys. Fluids 2015 for details on this case)  

(NB This case is operational but the way the curvature is computed is not optimal and should be improved...)

- POROUS_ROTATING_DISK

Flow over/across a porous disk. Work in progress.

- SQUARE_CONFINED

flow in a pipe with a square object. Work in progress.

- Example_Lshape :

A simple example to demonstrate the matlab/FreeFem exchange data format for a simple problem

### Test-cases currently not operational :

The next cases may not be operational with the present version and need to be updated to run with the latest version of StabFem.

- SPHERE_WORK :

(under development ; may not work)

- BIRDCALL :

(under work ; may not work due to recent changes in the nomenclature. If interested, get branch "Version2.0" instead of branch "Master").
 
- CYLINDER_VIV :
Study of the vortex-induced vibrations around a spring-mounted cylinder.

- STRAINED_BUBBLE :

Equilibrium shape and oscillation modes of a bubble within a uniaxial straining flow.





## Authors :

The Matlab part of the software is by D. Fabre, J. Sierra, D. Ferreira & M. Pigou (2017-).

The FreeFem part incorporates a number of sources from J. Tchoufag, P. Bonnefis, J. Mougel, V. Citro, F. Giannetti, O. Marquet, and many other students and collegues.

The plotting part of the interface uses the function ffpdeplot (alternative to pdeplot and fully compatible with Octave) developped by [Chloros](https://github.com/samplemaker/freefem_matlab_octave_plot). 


## Latest developments

- 06/2020 : stable version 3.5 with new generic interface and fully operational website
- 01/2020 : stable version 3.0 is released.
- 12/2019 : new management of the website of the project (in progress).
- 11/2019 : most issues with octave and windows are fixed.
- 10/2019 : an automatic non-regression runner is now operational : a number of "autorun.m" test-cases are automatically run at each new commit on the develop branch and/or each merge request.
- 09/2019 : new management of the database.
- 09/2019 : redesign of the automatic configuration (SF_core_start), including automatic checks for most systems (including windows 10).  
- 07/2019 : The project just moved to GitLab. The previous GitHub site is still operational but will soon not be maintained any more.
- 02/2019 : A mailing list is now available. To register please go [here](https://groupes.renater.fr/sympa/info/stabfem-dev) and click on "s'abonner".
- 02/2019 : A research paper advertising the software was published in Rev. Appl. Mech. [See here:](https://gitlab.com/stabfem/StabFem/blob/master/99_Documentation/ARTICLE_STABFEM/ARTICLE_ASME_Accepted.pdf)
- 2018 : a Manual is in progress. [See here:](https://github.com/erbafdavid/StabFem/blob/master/99_Documentation/MANUAL/main.pdf)
- 2018 : An automatic documentation using Doxygen is now available
- 2018 : Compatibility with Octave is now supported ! Test-cases "EXAMPLE_Lshape", "CYLINDER" and "ROTATING_POLYGONS" are successful. (a few points still may have to be fixed in other cases) 
- 2018 : Compatibility with windows 10 is now supported (check test-cases "EXAMPPLES_Lshape", "CYLINDER" and "POROUSDISK")
- 2018 : DNS is now possible within StabFem :)




