Gitlab pages principles:
Note : the layout of the website is based on the template "hyperspace" available at  https://html5up.net



- After each commit, a "gitlab-runner" executes the script located in
  REPOROOT/.gitlab-ci-page-script.sh
 which takes care of copying all "html" folders in STABLE and DEVELOPMENTS cases into
 the "public" folder. The whole "public" folder is then uploaded to "https://stabfem.gitlab.io/StabFem/"
 to generate a static HTML website for the project.

- To add your own case on the gitlab page, you should do the following :
  1 - Create a Matlab/octave script for your case, 
  2 - Click "Publish" to generate the html version of the script in a folder ./html (only available with matlab, not currently possible with Octave) 
  3 - Modify the ".gitlab-ci-page-script.sh" script to copy the new html folder into the public folder.
  4 - Modify the html sources of the website (i.e. index.html for the main page) to add a link toward the new case.

NB: If you desire to modify the website sources locally, you may execute manually the script .gitlab-ci-page-script.sh 
    to recreate the whole website sources on your local environment.

