function [] = SFcore_plotmode(~,~,filename,PlotSpectrumField,PlotModeOptions)
%>
%> This function is called internally in "Spectrum explorator" mode
%> (launched when clicking on an eigenvalue)
%> Added by DF on january 22,2020 after IMFT seminar...
%> 

    SF_core_log('n',' ... Ploting and eigenmode in Spectrum Explorator Mode ... ' ); 
    emPLOT = SFcore_ImportData(filename);
    
    if mod(length(PlotModeOptions),2)==1
            thefield = PlotModeOptions{1};
            PlotModeOptions = {PlotModeOptions{2:end}};
        else
            if strcmp(PlotSpectrumField,'default')
                if isfield(emPLOT,'ux')
                    thefield = 'ux';
                elseif isfield(emPLOT,'uz')
                    thefield = 'uz';
                elseif isfield(emPLOT,'p')
                      thefield = 'p';  
                else
                    SF_core_log('w',' You need to precise a fieldname using ''PlotSpectrumField'' ');
                end    
            else
                thefield = PlotSpectrumField;
            end
        end
    
    
    [plottitle1,plottitle2] = generateplottitle(emPLOT);
    plottitle = { plottitle1, plottitle2}; 
    figure; 
    
    SF_Plot(emPLOT,thefield,'title',plottitle,PlotModeOptions{:})  
    
    if isfield(emPLOT,'eta')
        [~,posmax] = max(abs(emPLOT.eta));
        E=0.2/emPLOT.eta(posmax);
        hold on;SF_Plot_ETA(emPLOT,'Amp',E,'style','r');hold off;
    end
end

function str = mycell2str(cell)
% This function will convert a cell array into a string 
% (for interactive spectrum explorer with option 'plotspectrum'='yes')
%
str = '';
for i = 1:length(cell)
    str = [str ','];
    if(ischar(cell{i}))
        str = [ str, '''' cell{i}  '''' ];
    end
    if(isnumeric(cell{i}))
        str = [ str, '[' num2str(cell{i}) ']' ];
    end
end
str = [str ''];
end


function [plottitle1,plottitle2] = generateplottitle(em)
    if isfield(em,'INDEXING') && isfield(em.INDEXING,'eigenvalue')
        eigenvalue = em.INDEXING.eigenvalue;
    elseif isfield(em,'INDEXING') && isfield(em.INDEXING,'lambda')
        eigenvalue = em.INDEXING.lambda;
    else
        eigenvalue = 0;
        SF_core_log('w','Could not found a field "eigenvalue" in metadata associated to eigenmode to generate the title of the figures')
    end
    plottitle1 =  [em.datatype, ' : ',num2str(eigenvalue)];
    plottitle2 = '';
    if isfield(em,'INDEXING')
        indexnames = fieldnames(em.INDEXING);
        for ind = indexnames'
            if ~strcmp(ind{1},'eigenvalue')
               plottitle2 = [plottitle2,ind{1},' = ',num2str(em.INDEXING.(ind{1})) ' ; '];
            end
        end
    plottitle2 = plottitle2(1:end-3);
    end
end