function Data = SF_Launch(file, varargin)
% generic Matlab/FreeFem driver
%
% usage : mesh = SF_Launch('File.edp', {Param1, Value1, Param2, Value2, etc...})
%
% First argument must be a valid FreeFem++ script
%
% Couples of optional parameters/values comprise :
%   'Params' -> an array of (numerical) input parameters for the FreeFem++ script
%           for instance SF_Launch('File.edp','Params',[10 100]) will be
%           equivalent to running 'FreeFem++ File.edp' and entering successively
%           10 and 100 through the keyboard)
%   'Options' -> a string containing the optional arguments passed to
%           Freefem and interpreted with getARGV.
%           for instance SF_Launch('File.edp','Options','-option1 10 -option2 20')
%           will launch 'FreeFem++ File.edp -option1 10 -option2 20'
%   'Mesh' -> a mesh associated to the data
%           (either a mesh struct or the name of a file)
%   'BaseFlow' -> a baseflow associated to the data
%           (either a flowfield struct or the name of a file)
%           (NB if providing a baseflow it is not ncessary to provide a
%           mesh as themesh is a field of the baseflow object)
%   'DataFile' -> the name of the resulting file to be imported (default is
%   Data.txt)
%   'Store' -> a string corresponding to the name of the subfolder where
%                 the data files should be stored
%
%   'Type' -> a string specifying the type of computation for the FreeFe++ script (OBSOLETE ?)
%
%
%
% by D. Fabre ,  june 2017, redesigned dec. 2018 and may-june 2020.
%


p = inputParser;
addParameter(p, 'Params', []);
addParameter(p,'Options','');
addParameter(p, 'Mesh', 0);
addParameter(p, 'BaseFlow', 0);
addParameter(p, 'DataFile', 'Data.ff2m');
addParameter(p, 'Store', '');
addParameter(p, 'Type', 'none');

parse(p, varargin{:});
ffargument = p.Results.Options;

ffdir = SF_core_getopt('ffdir'); % to remove soon

SF_core_log('nn', ['### Starting SF_Launch ', file]);

if (isstruct(p.Results.Mesh))
    SF_core_log('d', 'Mesh passed as structure');
    ffmesh = p.Results.Mesh;
    meshname = SFcore_MoveDataFiles(ffmesh.filename,'mesh.msh','cp');
elseif (isstruct(p.Results.BaseFlow))
    SF_core_log('d', 'Baseflow passed as structure');
    bf = p.Results.BaseFlow;
    ffmesh = p.Results.BaseFlow.mesh;
    SFcore_MoveDataFiles(bf.mesh.filename,'mesh.msh','cp');
    SFcore_MoveDataFiles(bf.filename,'BaseFlow.txt','cp');
end

stringparam = [];
if (~strcmpi(p.Results.Type,'none'))
        stringparam = [p.Results.Type '  '];
end

if ~(exist(file,'file'))
    if(exist([ffdir file],'file'))
        file = [ffdir file];
    else
        error([' Error in SF_Launch : FreeFem++ program ' ,file, ' not found']);
    end
end

stringparam = ' ';
if ~isempty(p.Results.Params)
    for pp = p.Results.Params
        if isnumeric(pp)
            stringparam = [stringparam, num2str(pp), '  '];
        else
            stringparam = [stringparam, pp, '  '];
        end
    end
end

% Launch FreeFem
value = SF_core_freefem(file,'parameters',stringparam,'arguments',ffargument);



%% If requested : copy file in subfolder of database

if ~isempty(p.Results.Store)
    if ~exist([SF_core_getopt('ffdatadir'),'/',p.Results.Store],'dir')
       SF_core_log('w',[' Folder ' SF_core_getopt('ffdatadir'),'/',...
           p.Results.Store, ' mentioned for database storing does not exist ; creating it']);
       mkdir([SF_core_getopt('ffdatadir'),'/',p.Results.Store])
    end
    SF_core_log('N',['Storing result in folder ',p.Results.Store]);
    SFcore_AddMESHFilenameToFF2M(p.Results.DataFile,ffmesh.filename);    
    filename = SFcore_MoveDataFiles(p.Results.DataFile,p.Results.Store);
else
    filename = p.Results.DataFile;
end

% Import results

hadError = 0;
if ~hadError
    if isnumeric(p.Results.Mesh)&&isnumeric(p.Results.BaseFlow)
        Data = SFcore_ImportData(filename);
    else
        Data = SFcore_ImportData(ffmesh, filename);
    end
else
    Data = [];
end


end
