%% Oscillation modes of a partially filled cylindrical container
%
% The aim of this tutorial program is to demonstrate how to use
% StabFem for capillary oscillations of static free-surface problem.
%
% The investigated situation corresponds to a partially filled cylindrical
% container with radius R. we note L the height of liquid along the
% vertical wall (including the meniscus), gamma the surface tension, theta 
% the contact angle and rhog the product between gravity acceleration and liquid density.
%

%% Initialization
close all;
addpath('../../SOURCES_MATLAB')
SF_Start('verbosity',4);
SF_core_setopt('eigensolver','ARPACK');
SF_core_setopt('freefemexecutable','/usr/local/bin/FreeFem++-mpi361')

system('mkdir FIGURES');
figureformat = 'png';

%% Chapter 1 : Oscillation modes for the case of a flat surface
%

%%
% We first create a  mesh with rectangular shape (flat surface).
% See the Freefem++ program 
% < https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/SLOSHING/MeshInit_Vessel.edp MeshInit_Vessel.edp >
% to see how it works. 

R = 1; % radius of the vessel
L = 1; % Height of the vessel
density=80; % this is the mesh density in the initial mesh 
gamma = 0.01;
rhog = 1;

%freesurf = SF_Mesh('MeshInit_Vessel.edp','Params',[L density],'problemtype','3DFreeSurfaceStatic');
freesurf = SF_Init('MeshInit_Vessel.edp','Params',[L density],'problemtype','3DFreeSurfaceStatic');
freesurf = SF_Deform(freesurf,'P',0,'gamma',gamma,'rhog',rhog,'typestart','pined','typeend','axis');

gamma = 0.01;
thetaE = pi/4; % this is the contact angle used in Viola et al.
hmeniscus = sqrt(2*gamma*(1-sin(thetaE))); % this is the expected height of the meniscus (valid for large Bond)
P = -freesurf.rhog*hmeniscus; % pressure in the liquid at z=0 (altitude of the contact line) ; 
% the result will be to lower the interface by this ammount 

%%
% Starting with the previously computed "flat surface" mesh as an initial "guess", 
% we use the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Mesh_Deform.m SF_Mesh_Deform.m> 
% to compute a mesh with a free surface satisfying the Young-Laplace
% solution. 
% The driver launches the FreeFem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/Newton_Axi_FreeSurface_Static.edp Newton_Axi_FreeSurface_Static.edp>
% .

freesurf = SF_Deform(freesurf,'P',P,'gamma',gamma,'rhog',rhog,'typestart','pined','typeend','axis');

%%
% We plot this mesh :

figure;SF_Plot(freesurf,'mesh','symmetry','ys');


%%
% A few statistics about the free surface shape :
% 
zR = freesurf.mesh.zsurf(1) % altitude of the contact line
z0 = freesurf.mesh.zsurf(end) % altitude of the center
%Vol0 = freesurf.Vol % volume
%alphastart = freesurf.alpha(1)*180/pi % this should be 225 degrees (angle with respect to vertical = 45 degrees)

%%

%% Chapter 5 : meniscus (theta = 45?), VISCOUS, m=1
%
% NB in the viscous case, the driver  <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Stability.m SF_Stability.m>
% invokes the FreeFem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/StabAxi_FreeSurface_Viscous.edp StabAxi_FreeSurface_Viscous.edp>

nu = 1e-3;
m=1;sym =  'YA';% YS if m is even, YA if m is odd

[~,emm1TOADAPT] =  SF_Stability(freesurf,'nev',10,'m',1,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis');
freesurf= SF_Adapt(freesurf,emm1TOADAPT(1));

[evm0VISCOUS,emm0] =  SF_Stability(freesurf,'nev',10,'m',0,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis','plotspectrum',true);
[evm1VISCOUS,emm1] =  SF_Stability(freesurf,'nev',10,'m',1,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis','plotspectrum',true);
[evm2VISCOUS,emm2] =  SF_Stability(freesurf,'nev',10,'m',2,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis','plotspectrum',true);

 % gamma = 0.01 / nu = 0.001

%  -0.0339 + 1.4555i
%  -0.0776 + 2.7738i
%  -0.1654 + 4.0398i

% After adapt

%  -0.0703 + 2.7975i
%  -0.0233 + 1.4669i
%  -0.1580 + 4.0691i

%%
P = 0
gamma = 0.1
nu = 0.1
freesurf = SF_Init('MeshInit_Vessel.edp','Params',[L density],'problemtype','3DFreeSurfaceStatic');
freesurf = SF_Deform(freesurf,'P',P,'gamma',gamma,'rhog',rhog,'typestart','pined','typeend','axis');
[evm1VISCOUS,emm1] =  SF_Stability(freesurf,'nev',10,'m',1,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis','plotspectrum',true);

%VP numero 0 : -0.90645+i1.89316
% VP numero 1 : 7.54415e-11+i2.22045e-15
% VP numero 2 : 5.8566e-11+i2.66454e-15
% VP numero 3 : -2.99328+i3.39646
% VP numero 4 : -2.66367+i0

%Ca colle !

% 1 / 1
% VP numero 0 : -1.87378+i-3.9968e-15
% VP numero 1 : 7.5706e-11+i-4.44089e-16
% VP numero 2 : 5.85587e-11+i1.77636e-15
% VP numero 3 : -3.63772+i-7.54952e-15
% VP numero 4 : -5.15554+i-1.46549e-14


%%
P =  -0.0765
gamma = 0.01
nu = 0.1
freesurf = SF_Init('MeshInit_Vessel.edp','Params',[L density],'problemtype','3DFreeSurfaceStatic');
freesurf = SF_Deform(freesurf,'P',P,'gamma',gamma,'rhog',rhog,'typestart','pined','typeend','axis');
[evm1VISCOUS,emm1] =  SF_Stability(freesurf,'nev',10,'m',1,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis','plotspectrum',true);
%VP numero 0 : -0.952654+i1.06005
% VP numero 1 : 1.20086e-05+i5.77316e-15
% VP numero 2 : 5.5737e-11+i5.32907e-15
% VP numero 3 : -0.541713+i-6.66134e-15
% VP numero 4 : -0.846743+i6.66134e-15
