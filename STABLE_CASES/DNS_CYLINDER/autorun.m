function value = autorun(varargin)
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE
%

close all;

if(nargin==0)
    verbosity=0;
  else
    verbosity= varargin{1};
end



SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');

value =0;
    
%%
    ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-40 80 40],'cleanworkdir',true);
    bf=SF_BaseFlow(ffmesh,'solver','Newton_2D.edp','Re',1,'Symmetry','S');
    bf=SF_BaseFlow(bf,'Re',10);
    bf=SF_BaseFlow(bf,'Re',60);
    
    Mask = SF_Mask(bf.mesh,[-2 10 0 2 .1]);
    bf = SF_Adapt(bf,Mask,'Hmax',5);

    bf = SF_Mirror(bf);
    bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Re',60,'Symmetry','N');
    
    % first test successul : mesgh adapt/mirror
    
    uxtest = SF_ExtractData(bf,'uy',2,.5)
    uxref=  -0.0312
    SFerror(1)=abs(uxtest/uxref-1)
    
%%
[ev,em] = SF_Stability(bf,'solver','Stab_2D.edp','shift',0.04+0.76i,'sym','N','nev',1); % compute the eigenmode. 
startfield = SF_Add(bf,em,'coefs',[1 0.01]); % creates startfield = bf+0.01*em

    % second step successul : add
    uxtest = SF_ExtractData(startfield,'uy',2,.5)
    uxref=  -0.0148
    SFerror(2)=abs(uxtest/uxref-1)
    
    
%%
Nit = 100; iout = 20;dt = 0.02;
[DNSstats,DNSfields] = SF_DNS(startfield,'solver','TimeStepper_2D.edp','Re',60,'Nit',Nit,'dt',dt,'iout',iout);


FyendREF = 4.3816e-04
uxtestREF = -0.019848150609038  %-0.024544154712794

Fyend = DNSstats.Fy(end)
uxtest = SF_ExtractData(DNSfields(end),'uy',2,.5)

SFerror(3) = abs(Fyend/FyendREF-1)

sfs = SF_Status
field = SF_Load('DNSFIELDS','last');
bf = SF_Load('BASEFLOWS','last');

uxtest = SF_ExtractData(field,'uy',2,.5)
SFerror(4) = abs(uxtest/uxtestREF-1)


value = sum(SFerror>1e-2)

end
%[[AUTORUN:SHORT]]