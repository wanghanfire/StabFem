function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE
%
% USAGE : 
% autorun(0) -> automatic check
% autorun(1) -> produces the figures used for the manual
% autorun(2) -> may produces "bonus" results...
%%
close all;

if(nargin==0) 
    verbosity=0; 
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');

value =0;

%% Test 1 : base flow
bf = SmartMesh_L6();

disp(['Test 1 : Drag : ', bf.Cx]);
CxREF = 3.2618

SFerror(1) = abs(bf.Cx/CxREF-1)






%% Test 2 : eigenvalues 
[ev,em] = SF_Stability(bf,'m',1,'shift',.2+0.3i,'nev',20,'type','D','sort','LR') ;  

%evREF = [     0.0501 + 0.6574i 0.2425 + 0.0000i ]
ev(1:2)
%evREF = [0.2426 + 0.0000i, 0.0504 + 0.6587i]

evREF = [ 0.330502 , 0.174735 + 0.583378i ]

SFerror(2) = abs(ev(1)/evREF(1)-1)+abs(ev(2)/evREF(2)-1)


%% Test 3 : structural sensitivity
[ev,emD] = SF_Stability(bf,'m',1,'shift',0.0504 + 0.6587i,'nev',1,'type','D') ;  
[ev,emA] = SF_Stability(bf,'m',1,'shift',0.0504 + 0.6587i,'nev',1,'type','A') ; 
S = SF_Sensitivity(bf,emD,emA);

Sprobe = SF_ExtractData(S,'sensitivity',7,.5)
SprobeREF =  0.3868 %0.154424018603376

SFerror(3) = abs(SprobeREF/Sprobe-1)




%% Test 4 : mesh stretch

bf2 = SF_MeshStretch(bf,'Yratio',1.1,'Ymin',0.5);

bf2.Cx
CxREF = 2.8379

SFerror(4) = abs(bf2.Cx/CxREF-1)


% tests 5 et 6 : SLEPC versus ARPACK

if SF_core_detectlib('SLEPc-complex')
    SF_core_setopt('eigensolver','ARPACK')
    tic 
    [ev,em] = SF_Stability(bf,'m',1,'shift',.2+0.3i,'nev',5,'type','D','sort','LR') ;  
    SFerror(5) = abs(ev(1)/evREF(1)-1)+abs(ev(2)/evREF(2)-1)
    disp('Time spent with ARPACK :');
    toc
    
    tic
    SF_core_setopt('eigensolver','SLEPC')
    [ev,em] = SF_Stability(bf,'m',1,'shift',.2+0.3i,'nev',5,'type','D','sort','LR') ;  
    SFerror(6) = abs(ev(1)/evREF(1)-1)+abs(ev(2)/evREF(2)-1)
    disp('Time spent with SLEPC :');
    toc
end


value = sum(SFerror>1e-2)


end

 

