function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of a cylinder with STABFEM  
%  - Base flow computation
%  - Linear stability
%  - WNL
%  - Harmonic Balance
%
% USAGE : 
% autorun -> automatic check (non-regression test). 
% Result "value" is the number of unsuccessful tests
% autorun(1) -> produces the figures (in present case not yet any figures)
%
% [[AUTORUN:PERIODIC]] % tag to launch this autorun through gitlab runner
%
  
if(nargin==0) 
    verbosity=0; 
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');
SF_core_setopt('eigensolver','default')
SF_core_arborescence('cleanall');
LsurD = 2;
ffmesh = SF_Mesh('Mesh_Cylinder_Periodic.edp','Params',LsurD,'problemtype','2d')    

bf = SF_BaseFlow(ffmesh,'Re',1,'Omegax',.5);
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',60,'Omegax',.5);
bf = SF_Adapt(bf);

figure ; SF_Plot(bf,'ux','xlim',[-2 2])

Fy_REF = bf.Fy
SFerror(1) = abs(bf.Fy/Fy_REF-1)

[ev,em] = SF_Stability(bf,'nev',10,'shift',2i);
ev(1)
evref = 0.1414 + 1.9257i

SFerror(2) = abs(ev(1)/evref-1)


figure ; SF_Plot(em(1),'vort','xlim',[-2 2],'colorrange','cropcenter')


value = sum((SFerror>1e-2))

end
