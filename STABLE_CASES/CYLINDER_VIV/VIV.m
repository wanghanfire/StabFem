%%  LINEAR Stability ANALYSIS of the wake of a SPRING-MOUNTED CYLINDER
%
% 
%  this scripts demonstrates how to use StabFem to study the instabilities
%  in a Vortex-Induced Vibrations (VIV) problem. 
%
%  The script reproduces figures 2c and 4a from the paper by D. Sabino et al.
%  
%

%%
%
% First we set a few global variables needed by StabFem
%
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB']);
SF_Start('verbosity',2);
close all;

%% Compute or import the base flow
% 

bf = SF_Load('lastadapted'); 

if isempty(bf)
    bf = SmartMesh_Cylinder;  
end


%% Eigenvalue computation : plot one mode
bf = SF_BaseFlow(bf,'Re',60);
mstar = 20; Ustar = 3;shiftFM = 0.04689 + 0.74874i; 
M = mstar*pi/4; K = pi^3*mstar/Ustar^2;
[ev,em] = SF_Stability(bf,'shift',shiftFM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0,'plotspectrum',true);

figure ; SF_Plot(em(1),'ux','xlim',[-2 4],'ylim', [0 2],'colorrange','cropcentered');


%% Loop on eigenvalue computation
% starting points
shiftFM = 0.04689 + 0.74874i; 
shiftEM = -0.0188 + 2.0100i; 


% computing branch "FM" for a range of Ustar

Ustar_tab = [3:.2:11];
K = pi^3*mstar/Ustar_tab(1)^2;
ev = SF_Stability(bf,'shift',shiftFM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);

for k = 1:length(Ustar_tab)
  Ustar = Ustar_tab(k);
  K = pi^3*mstar/Ustar^2;
  ev = SF_Stability(bf,'shift','cont','nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);
  FM_tab(k) = ev;
end


% computing branch "EM" for a range of Ustar

Ustar_tab = [3:.2:11];
K = pi^3*mstar/Ustar_tab(1)^2;
ev = SF_Stability(bf,'shift',shiftEM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);

for k = 1:length(Ustar_tab)
  Ustar = Ustar_tab(k);
  K = pi^3*mstar/Ustar^2;
  ev = SF_Stability(bf,'shift','cont','nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);
  EM_tab(k) = ev;
end

% plotting both these branches

figure;
subplot(2,1,1);
plot(Ustar_tab,real(FM_tab),'b');hold on;
plot(Ustar_tab,real(EM_tab),'r');
xlabel('U*'); ylabel('\lambda_r');
legend('FM','EM');
subplot(2,1,2);
plot(Ustar_tab,imag(FM_tab)/(2*pi),'b');hold on;
plot(Ustar_tab,imag(EM_tab)/(2*pi),'r');
xlabel('U*'); ylabel('St');
legend('FM','EM');

%% Impedance computation

bf = SF_BaseFlow(bf,'Re',35);
close all;
fo = SF_LinearForced(bf,'omega',[0:.01:1],'plot','yes');
% [[PUBLISH]] (this tag is to enable automatic publication as html ; don't touch this line)
