addpath('../../SOURCES_MATLAB/');


%% Mesh M2 (improved)
SF_Start('ffdatadir','./CHI1_MESHM2/','verbosity',4)

sfs = SF_Status('ALL');
if strcmp(sfs.status,'existent') 
    bf = SF_Load('BASEFLOWS','last');
else

chi = 1;
ffmesh = SF_Mesh('Mesh_OneHole.edp','Params',[chi,15,20,10,10],'problemtype','AxiXR');
%ffmesh = SF_SetMapping(ffmesh,'mappingtype','jet','mappingparams',Params);

bf = SF_BaseFlow(ffmesh,'Re',1,'type','new');
bf = SF_Adapt(bf,'Hmax',3); 
bf = SF_BaseFlow(bf,'Re',30);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf,'Hmax',3); 
bf = SF_BaseFlow(bf,'Re',300);
bf = SF_BaseFlow(bf,'Re',1000);
bf = SF_Adapt(bf,'Hmax',3);

bf = SF_BaseFlow(bf,'Re',1500);
bf = SF_Adapt(bf,'Hmax',3);

Params = [5 1e30 1 0.5 20 1e30];
Omega1 = .5/chi;
ForcedFlow1 = SF_LinearForced(bf,Omega1,'mappingdef','jet','mappingparams',Params);
%Omega2 = 2.1/chi;
%ForcedFlow2 = SF_LinearForced(bf,Omega2,'mappingdef','jet','mappingparams',Params);
Omega3 = 4.5/chi;
ForcedFlow3 = SF_LinearForced(bf,Omega3,'mappingdef','jet','mappingparams',Params);

Mask = SF_Mask(bf.mesh,[-3*chi chi 0 1.5 .1]);
Mask2 = SF_Mask(bf.mesh,[0 10 0 3 .3]);

bf = SF_Adapt(bf,Mask,Mask2,ForcedFlow3,ForcedFlow1,'Hmax',4);

bf = SF_BaseFlow(bf,'Re',1600);
end
bfM2 = bf;
%% Computes eigenmodes

Params = [5 1e30 1 0.5 20 1e30];
[ev,emH2] = SF_Stability(bf,'m',0,'nev',1,'shift',-2.1i,'mappingdef','jet','mappingparams',Params);
figure;SF_Plot(emH2,'p','xlim',[-3 20],'ylim',[0 4],'colormap','redblue','logsat',1);

[ev,emH3] = SF_Stability(bf,'m',0,'nev',1,'shift',-.09-4.1i,'mappingdef','jet','mappingparams',Params);
figure;SF_Plot(emH3,'p','xlim',[-3 20],'ylim',[0 4],'colormap','redblue','logsat',1);

evH1 = SF_Stability(bf,'m',0,'nev',1,'shift',-.11-.5i,'mappingdef','jet','mappingparams',Params);


%% Result with this mesh for Re = 2000
bf = SF_BaseFlow(bf,'Re',2000);
evA = SF_Stability(bf,'m',0,'nev',1,'shift',-0.0450 - 0.5610i,'mappingdef','jet','mappingparams',Params)
evB = SF_Stability(bf,'m',0,'nev',1,'shift',0.3010 - 2.2434i,'mappingdef','jet','mappingparams',Params);
evC = SF_Stability(bf,'m',0,'nev',1,'shift',0.2408 - 4.3205i,'mappingdef','jet','mappingparams',Params);



%% MESH M4 (improved)
SF_core_setopt('ffdatadir','CHI1_MESH_M4'); 
sfs = SF_Status('ALL');
if strcmp(sfs.status,'existent') 
    bf = SF_Load('BASEFLOWS','last');
else
chi = 1;
ffmesh = SF_Mesh('Mesh_OneHole.edp','Params',[chi,15,60,10,10],'problemtype','AxiXR');
%ffmesh = SF_SetMapping(ffmesh,'mappingtype','jet','mappingparams',Params);

bf = SF_BaseFlow(ffmesh,'Re',1,'type','new');
bf = SF_Adapt(bf,'Hmax',2); 
bf = SF_BaseFlow(bf,'Re',30);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf,'Hmax',2); 
bf = SF_BaseFlow(bf,'Re',300);
bf = SF_BaseFlow(bf,'Re',1000);
bf = SF_Adapt(bf,'Hmax',2);

bf = SF_BaseFlow(bf,'Re',1500);
bf = SF_Adapt(bf,'Hmax',2);

Params = [5 1e30 1 0. 20 1e30];
Omega1 = .5/chi;
ForcedFlow1 = SF_LinearForced(bf,Omega1,'mappingdef','jet','mappingparams',Params);
Omega2 = 2.1/chi;
ForcedFlow2 = SF_LinearForced(bf,Omega2,'mappingdef','jet','mappingparams',Params);
Omega3 =5/chi;
ForcedFlow3 = SF_LinearForced(bf,Omega3,'mappingdef','jet','mappingparams',Params);

%Mask = SF_Mask(bf.mesh,[-2*chi chi 0 2 .1]);
%Mask2 = SF_Mask(bf.mesh,[-2-2*chi 20 0 3 .3]);

bf = SF_Adapt(bf,ForcedFlow3,ForcedFlow2,'Hmax',.25);

bf = SF_BaseFlow(bf,'Re',1600);
end

%% Computes eigenmodes

Params = [5 1e30 1 0. 20 1e30];
[ev,emH1M4] = SF_Stability(bf,'m',0,'nev',1,'shift',-.11-.5i,'mappingdef','jet','mappingparams',Params);

[ev,emH2M4] = SF_Stability(bf,'m',0,'nev',1,'shift',.08-2.1i,'mappingdef','jet','mappingparams',Params);
figure;SF_Plot(emH2M4,'p','xlim',[-3 20],'ylim',[0 4],'colormap','redblue','logsat',1);

[ev,emH3M4] = SF_Stability(bf,'m',0,'nev',1,'shift',-.09-4.1i,'mappingdef','jet','mappingparams',Params);
figure;SF_Plot(emH3M4,'p','xlim',[-3 20],'ylim',[0 4],'colormap','redblue','logsat',1);


%% Result with this mesh for Re = 2000
SF_core_setopt('ffdatadir','CHI1_MESH_M4'); 
bf = SF_Load('BASEFLOWS','last');
bf = SF_BaseFlow(bf,'Re',2000);
evA = SF_Stability(bf,'m',0,'nev',1,'shift',-0.0450 - 0.5610i,'mappingdef','jet','mappingparams',Params)
evB = SF_Stability(bf,'m',0,'nev',1,'shift',0.3010 - 2.2434i,'mappingdef','jet','mappingparams',Params);
evC = SF_Stability(bf,'m',0,'nev',1,'shift',0.2408 - 4.3205i,'mappingdef','jet','mappingparams',Params);



%% FINAL FIGURES
close all;
figure;
SF_Plot(bfM2,'mesh','xlim',[-3 20],'ylim',[-4 4]);
hold on;SF_Plot(emH2M4,'mesh','xlim',[-3 20],'ylim',[-4 4],'symmetry','XM');
plot([5 5],[0 4],'k--','LineWidth',2);
text(5,4.3,'$x=L_m$','interpreter','latex');
pos = get(gcf,'Position'); pos(3) = 600;pos(4)=pos(3)*.4;set(gcf,'Position',pos);            
saveas(gcf,'Mesh_Appendix','png');
saveas(gcf,'Mesh_Appendix','tif');
pause(0.1);
%%
figure; SF_Plot(emH2,'p','xlim',[-3 18],'ylim',[-4 4],'colormap','redblue','logsat',1,'boundary','on','bdlabels',2,'bdcolors','k');
hold on;SF_Plot(emH2M4,'p','xlim',[-3 18],'ylim',[-4 4],'colormap','redblue','logsat',1,'symmetry','XM','boundary','on','bdlabels',2,'bdcolors','k');
plot([5 5],[0 4],'k--','LineWidth',2);
text(5,4.3,'$x=L_m$','interpreter','latex');
pos = get(gcf,'Position'); pos(3) = 600;pos(4)=pos(3)*.4;set(gcf,'Position',pos);            
saveas(gcf,'Mode_Appendix','png');
saveas(gcf,'Mode_Appendix','tif');
pause(0.1);

%%
figure; SF_Plot(emH3,'p','xlim',[-3 18],'ylim',[-4 4],'colormap','redblue','boundary','on','bdlabels',2,'bdcolors','k');
hold on;SF_Plot(emH3M4,'p','xlim',[-3 18],'ylim',[-4 4],'colormap','redblue','symmetry','XM','boundary','on','bdlabels',2,'bdcolors','k');
plot([5 5],[0 4],'k--','LineWidth',2);
text(5,4.3,'$x=L_m$','interpreter','latex');
pos = get(gcf,'Position'); pos(3) = 600;pos(4)=pos(3)*.4;set(gcf,'Position',pos);            
saveas(gcf,'ModeH3','png');
saveas(gcf,'ModeH3','tif');
pause(0.1);
