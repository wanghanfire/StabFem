%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%%
load('convergedHBNSponge.mat');
%% WNL
%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%%
nu0 = 1.466e-5; D0 = 2*1.24e-3; T0=288.5; gamma=1.4; R=288;
Re0=500; U0=Re0*nu0/(D0); c0 = sqrt(gamma*R*T0); 
Ma = U0/c0;
Ma=0.02;
ReList = [10,35,50,75,100,150,200,250,300,350,400];
% Create initial mesh
ffmesh = SF_Mesh('Mesh_BirdCall_Sponge.edp','problemtype','axicompsponge');
bf=SF_BaseFlow(ffmesh,'Re',1,'Mach',Ma);
bf=SF_Adapt(bf,'Hmax',10);
for Re = ReList
    bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW');
    bf=SF_Adapt(bf,'Hmax',6.5);
end
%% Mesh adaptation to sensitivity
ffmesh = SF_SetMapping(ffmesh, 'MappingType', 'box', 'MappingParams', [-5,10,0,5,-0.0,2.5,-0.0,2.5]) 
[ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',ev(1),'nev',5,'type','A','m',0);
sensitivity = SF_Sensitivity(bf, em(1), emA(1), 'Type','S');
MaskCav = SF_Mask(bf.mesh,[-1 3.0 0 4 .1]);
MaskWake = SF_Mask(bf.mesh,[0.0 20.0 -1 1 .05]);
bf=SF_Adapt(bf,sensitivity,MaskCav,MaskWake,'Hmax',8);
[ev,em] = SF_Stability(bf,'shift',ev(1),'nev',5,'type','D','m',0);
%% WNL
Rec=337.8; Ma=0.02;
bf=SF_BaseFlow(bf,'Re',Rec,'Mach',Ma,'type','NEW');
[ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',conj(ev(1)),'nev',5,'type','A','m',0);
ReT=Rec+0.25;
[wnl,meanflow,mode,mode2] = SF_WNL(bf,em(1),'Retest',ReT,'Adjoint',emA(1),'Normalization','none');
sensitivity = SF_Sensitivity(bf, em(1), emA(1), 'Type','S');
MaskCav = SF_Mask(bf.mesh,[-1 3.0 0 4 .1]);
MaskWake = SF_Mask(bf.mesh,[0.0 20.0 -1 1 .1]);
bf=SF_Adapt(bf,sensitivity,MaskCav,MaskWake,'Hmax',6);
[ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',conj(ev(1)),'nev',5,'type','A','m',0);
ReT=Rec+0.05;
[wnl,meanflow,mode,mode2] = SF_WNL(bf,em(1),'Retest',ReT,'Adjoint',emA(1),'Normalization','none');

%% 
% PLOTS of WNL predictions
% Here we plot only the lift force, other quantities will be plotted at the
% end and compared with HB

Omegac=imag(em(1).lambda); Rec=em(1).Re;
epsilon2_WNL = 0:.00001:.00005; % will trace results for Re = 40-55 approx.
Re_WNL = 1./(1/ReInt(1)-epsilon2_WNL);
A_WNL = wnl.Aeps*real(sqrt(epsilon2_WNL));
Fy_WNL = wnl.Fyeps*real(sqrt(epsilon2_WNL))*2; % factor 2 because of complex conjugate
omega_WNL =omegaNList(1)+ epsilon2_WNL*imag(wnl.Lambda) ...
                  - epsilon2_WNL.*real(wnl.Lambda)*imag(wnl.nu0+wnl.nu2++wnl.nu1)/real(wnl.nu0+wnl.nu2++wnl.nu1)  ;
Fx_WNL = wnl.Fx0 + wnl.Fxeps2*epsilon2_WNL  ...
                 + wnl.FxA20*real(wnl.Lambda)/real(wnl.nu0+wnl.nu2)*epsilon2_WNL.*(epsilon2_WNL>0) ;

Lambda=wnl.Lambda; Nu0=wnl.nu0; Nu1=wnl.nu1; Nu2=wnl.nu2; AAA2=(real(Lambda)/real(Nu0+Nu1+Nu2));

coeffomega=imag(Lambda) - imag(Nu0+Nu1+Nu2)*AAA2           
omega_WNL = omegaNList(1)+coeffomega*epsilon2_WNL;

figure(24); 
plot(ReInt,omegaNList,'ko');
hold on;
plot(Re_WNL,abs(omega_WNL),'g-','LineWidth',2);
xlabel('Re');ylabel('\omega')

figure(25); 
plot(ReInt,sqrt(AEnergy1HB3.^2 + AEnergy2HB3.^2 + AEnergy3HB3.^2) );
hold on; 
plot(Re_WNL,10*(real(sqrt(epsilon2_WNL))),'g--','LineWidth',2);
xlabel('Re');ylabel('A')

pause(0.1);