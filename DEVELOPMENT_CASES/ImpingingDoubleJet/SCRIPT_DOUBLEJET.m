
%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',20,'workdir','./WORK/'); % verbosity 4 to see what is sufficient
mkdir('FIGURES');

%% Parameters 
global sfopts;
LpipeC = 10; RoutC = 80; % ,nb first case was done with Rout = 120 !
RcMin = 5; RcMax = 5; RcStep = 2;
HMin  = 10; Hmax  = 10; HStep  = 0.5;
ReTarget = 70; % Bigger than 110
ReMin = 60; ReMax = ReTarget; ReStep = 10;
RcArray = [RcMin:RcStep:RcMax];
HArray  = [HMin:HStep:Hmax];
ReArray  = [ReMin:ReStep:ReMax];
N = length(RcArray); M = length(HArray); NReyCase = length(ReArray);
EVArray = nan*ones(NReyCase);
EV2Array = nan*ones(NReyCase);
EV3Array = nan*ones(NReyCase);
ListMeshRef = linspace(5,1,M);
%% e-Rc Stationary stability map for Re = ReTarget 

for i=[1:M]
    meshRef = ListMeshRef(i);
    for j=[1:N]
        HC = HArray(i);
        RcC = RcArray(j);
        nameffdatadir = ['./WORK/H_',num2str(HC),'_Rc_',num2str(RcC),'/'];
        SF_core_start('verbosity',4,'workdir',nameffdatadir);
        bf = meshGeneration('Rc',RcC,'H',HC,'Rout',RoutC,'Lpipe',LpipeC,...
                            'ReEnd',ReTarget,'meshRef',meshRef,'ReFirstStep',...
                            10,'ReSectStep',10);
        for n=[NReyCase:-1:1]
            Re = ReArray(n);
            bf = SF_BaseFlow(bf,'Re',Re);
            [ev,em] = SF_Stability(bf,'nev',3,'shift',0.1+0.i,'k',0,'type','D','sym','N');
            EVArray(n) = ev(1);
            EMArray(n) = em(1);
            EV2Array(n) = ev(2);
            EM2Array(n) = em(2);
            EV3Array(n) = ev(3);
            EM3Array(n) = em(3);
            BFArray(n) = bf;
        end
        save([nameffdatadir,'save_',num2str(HC),'_Rc_',num2str(RcC),'.mat'],...
        'EVArray','EV2Array','EV3Array','EMArray','EM2Array','EM3Array',...
        'BFArray','RcC','HC','LpipeC','RoutC','i','j','nameffdatadir'); 
    end
end

