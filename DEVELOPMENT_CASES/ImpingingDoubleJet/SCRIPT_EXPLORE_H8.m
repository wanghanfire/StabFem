addpath('../../SOURCES_MATLAB/');
H=8;Rc=12;Lpipe=10;Rout=200;meshRef=5;ReEnd=300;
SF_core_start('verbosity',4,'workdir',['./WORK/H',num2str(H),'Rc',num2str(Rc),'/']); % verbosity 4 to see what is sufficient
ffmesh = SF_Mesh('mesh_DoubleJet.edp','Params',[H Rc Lpipe Rout],'problemtype','2D');
bf = SF_BaseFlow(ffmesh,'Re',1);   
bf = SF_Adapt(bf,'Hmax',meshRef);

for Re = [10:5:130] 
    bf = SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf,'Hmax',meshRef);
end

for Re = [127.5:2.5:200] 
    bf = SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf,'Hmax',meshRef);
end

%%
[evSA,emSA] = SF_Stability(bf,'nev',5,'shift',0.1+0.i,'k',0,'type','D','sym','A');
[evSS,emSS] = SF_Stability(bf,'nev',5,'shift',0.1+0.i,'k',0,'type','D','sym','S');
bf = SF_Adapt(bf,emSA(1),emSA(2),emSS(1),'Hmax',meshRef);
%%

%bf = SF_Split(bf,'nsplit',2);
bf = SF_Mirror(bf);
%%
i = 1;
j = 1;
for Re = [200:-1.0:60] 
    bf = SF_BaseFlow(bf,'Re',Re);
    [evS,emS] = SF_Stability(bf,'nev',10,'shift',0.1+0.i,'k',0,'type','D','sym','N');
    [evU,emU] = SF_Stability(bf,'nev',30,'shift',0.3+0.5i,'k',0,'type','D','sym','N');
    evSList(i:i+9) = evS;
    evUList(j:j+29) = evU;
    i = i + 10;
    j = j + 30;
end

