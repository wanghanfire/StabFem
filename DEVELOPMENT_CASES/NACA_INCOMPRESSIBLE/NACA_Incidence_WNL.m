%% WNL analysis of steady incompressible laminar flow around a NACA 12 wing profile at small incidence 
%
% This script compute the steady flows (base flow) around a NACA 12 wing for Re = 2000 and incidence
% alpha in [0,10 deg] , and compares with WNL analysis 
%%


%% CHAPTER 0 : set the global variables needed by the drivers


close all;
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB/']);
SF_Start('verbosity',4); % set to 4 if you want to follow freefem execution
SF_core_setopt('ErrorIfDiverge',false); % Recommended option if divergence errors are correctly handled using 'iter<0' tests

figureformat='tif'; AspectRatio = 0.56; % for figures
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
tic;


if ~exist('Chapter1Done')
%% CHAPTER 1 : Creates an initial mesh
SF_core_arborescence('cleanall');
Rout = 30;
HMax = 1;
np = 128;
ffmesh = SF_Mesh('Naca0012MeshFull.edp','Params',[Rout,np]);
bf=SF_BaseFlow(ffmesh,'solver','Newton_2D.edp','Re',10,'alpha',0);

%%
% Some plots

figure; subplot(2,1,1);SF_Plot(bf,'mesh','title',' Initial Mesh'); subplot(2,1,2);SF_Plot(bf,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');

figure; SF_Plot(bf,'p','xlim',[-.2 1.2],'ylim', [-.4 .4],'colormap','redblue','title','Re=10,alpha=0 : pressure and velocity');
hold on; SF_Plot(bf,{'ux','uy'},'xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');


%% Chapter 2 : raise Re to 2000 and readapt mesh

alpha = 0;
Re_tab = [10 30 100 200 500 1000 2000 ];
for Re = Re_tab
    bf=SF_BaseFlow(bf,'Re',Re,'alpha',alpha);
     if bf.iter<0
        disp(['Warning : Newton failed for Re = ',num2str(Re)]);
        break
    end
    bf=SF_Adapt(bf);
end
disp([' Number of cells in the adapted mesh : ',num2str(bf.mesh.np)]); 
Chapter1Done = true;
else
     bf = SF_Load('BASEFLOWS',14); % load BF number 14
end
bf0 = bf;

%% 
% some figures
figure; subplot(2,1,1);SF_Plot(bf,'mesh','title','Mesh'); subplot(2,1,2);SF_Plot(bf,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');

figure; SF_Plot(bf,'p','xlim',[-.2 1.2],'ylim', [-.4 .4],'colormap','redblue','title','Re=2000,alpha=0 : pressure and velocity');
hold on; SF_Plot(bf,{'ux','uy'},'xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');



%% Chapter 3 : loop on alpha 
alphatab = .1:.5:10;
for j=1:length(alphatab)
    alpha = alphatab(j);
    bf = SF_BaseFlow(bf,'alpha',alpha);
    bf = SF_Adapt(bf);
    Fxtab(j) = bf.Fx;
    Fytab(j) = bf.Fy;
end


%% Chapter 4 : WNL computation 
 bf = bf0;

WNL =  SF_Launch('WNL_Incidence.edp','Baseflow',bf)

 
%% Figures
figure(11);plot(alphatab,Fytab,'b-x',alphatab,WNL.Fy1*(alphatab*pi/180)+WNL.Fy3*(alphatab*pi/180).^3,'r--');
xlabel('\alpha (^o)');ylabel('Fy');legend('NL','WNL');
 
figure(12);plot(alphatab,Fxtab,'b-x',alphatab,WNL.Fx0+WNL.Fx2*(alphatab*pi/180).^2,'r--');
xlabel('\alpha (^o)');ylabel('Fx');legend('NL','WNL');


%% Chapter 5 : Try with Half-mesh
SF_core_setopt('verbosity',4)
ffmesh = SF_Mesh('Naca0012MeshHalf.edp','Params',[Rout,np]);
bf=SF_BaseFlow(ffmesh,'solver','Newton_2D.edp','Re',10,'alpha',0,'Symmetry','S');


%% raise Re to 2000 and readapt mesh

alpha = 0;
Re_tab = [30 100 200 500 1000 2000 ];
for Re = Re_tab
    bf=SF_BaseFlow(bf,'Re',Re,'alpha',alpha);
     if bf.iter<0
        disp(['Warning : Newton failed for Re = ',num2str(Re)]);
        break
    end
    bf=SF_Adapt(bf);
end
disp([' Number of cells in the adapted mesh : ',num2str(bf.mesh.np)]); 
bf0 = bf;

%%
% Some plots

figure; subplot(2,1,1);SF_Plot(bf,'mesh','title','Mesh'); subplot(2,1,2);SF_Plot(bf,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');

figure; SF_Plot(bf,'p','xlim',[-.2 1.2],'ylim', [-.4 .4],'colormap','redblue','title','Re=2000,alpha=0 : pressure and velocity');
hold on; SF_Plot(bf,{'ux','uy'},'xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');



%% WNL and figures
WNLh = SF_Launch('WNL_Incidence.edp','Baseflow',bf,'Options','-Symmetry S');

figure(11);hold on;plot(alphatab,WNLh.Fy1*(alphatab*pi/180)+WNLh.Fy3*(alphatab*pi/180).^3,'g-.');
xlabel('\alpha (^o)');ylabel('Fy');legend('NL','WNL', 'WNL (half mesh)');
 
figure(12);hold on; plot(alphatab,WNLh.Fx0+WNLh.Fx2*(alphatab*pi/180).^2,'g-.');
xlabel('\alpha (^o)');ylabel('Fx');legend('NL','WNL','WNL (half mesh)');

%% Summary 
SF_Status

 
%
% [[PUBLISH]]