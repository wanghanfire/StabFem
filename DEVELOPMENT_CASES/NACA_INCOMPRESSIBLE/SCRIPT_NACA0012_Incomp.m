%% Incompressible laminar flow around a NACA 12 wing profile at incidence.
%
% This script demonstrates how to use StabFem to compute the steady flow
% (base flow) around a NACA 12 wing profile at incidence, and demonstrates the usage of
% mesh adaptation features.
%%


%% CHAPTER 0 : set the global variables needed by the drivers


close all;
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB/']);
SF_Start('verbosity',2); % set to 4 if you want to follow freefem execution
SF_core_arborescence('cleanall');
SF_core_setopt('ErrorIfDiverge',false); % Recommended option if divergence errors are correctly handled using 'item<0' tests

figureformat='tif'; AspectRatio = 0.56; % for figures
system('mkdir FIGURES');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
tic;

%% CHAPTER 1 : Creates an initial mesh
Rout = 30;
HMax = 1;
np = 128;
ffmesh = SF_Mesh('Naca0012MeshFull.edp','problemtype','2d','Params',[Rout,np]);
bf=SF_BaseFlow(ffmesh,'Re',10,'alpha',10);

%%
% Some plots

figure; subplot(2,1,1);SF_Plot(bf,'mesh','title','initial mesh'); subplot(2,1,2);SF_Plot(bf,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');

figure; SF_Plot(bf,'p','xlim',[-.2 1.2],'ylim', [-.4 .4],'colormap','redblue','title','Re=10,alpha=10 : pressure and velocity');
hold on; SF_Plot(bf,{'ux','uy'},'xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');


%% Chapter 2 : raise Re to 5000 and readapt mesh

alpha = 10;
Re_tab = [10 30 100 200 500 1000 2000 5000 ];
for Re = Re_tab
    bf=SF_BaseFlow(bf,'Re',Re,'alpha',alpha);
     if bf.iter<0
        disp(['Warning : Newton failed for Re = ',num2str(Re)]);
        break
    end
    bf=SF_Adapt(bf);
end
disp([' Number of cells in the adapted mesh : ',num2str(bf.mesh.np)]); 

%% Explanations on mesh adaptation parameters
%
% SF_Adapt.m is a interface to the generic mesh adapter AdaptMesh.edp which
% can adapt to up to 13 fields with a number of possible storage modes (P2P2P1, P2P2P2P1, etc...)
% The main options controling the efficiency of mesh adaptation, and the default values in the StabFem implementation 
% are as follows:
%
% - *'Hmax'*  max cell size  _(Default : 0.033 sqrt(Area)  )_
%
% - *'Hmin'*  min cell size  _(Default : 1e-4 sqrt(Area) )_
%
% - *'Nbvx'*  max number of points  _(Default : 1e5 )_
%
% - *'InterpError'* Interpolation error  _(Default : 0.01 )_
%
% - *'anisomax'* Parameter controlling anisotropy ; 1=isotropic, inf=very anisotropic  _(Default : 10 )_
% 
% - *'nbjacoby'* Parameter controlling variation of size among adjacent cells ; 
%
%   0= derefinement can be abrupt ; large = derefinement is more progressive  _(Default : 6 )_
%
%   NB after mesh adaptation, the baseflow is automatically recomputed on
%   the new mesh, unless you use option 'recompute',false
%
%   These default parameters are used in this section;  effect of some parameters is
%   investigated in chapter 4.
%
%

%%
% Some plots


figure; subplot(2,1,1);SF_Plot(bf,'mesh','title','Adapted mesh for Re=5000','xlim',[-2 4],'ylim',[-2 2]); subplot(2,1,2);SF_Plot(bf,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');

figure; SF_Plot(bf,'p','xlim',[-.2 1.2],'ylim', [-.4 .4],'colormap','redblue','title',['Re=',num2str(bf.Re),',alpha=10 : pressure and velocity']);
hold on; SF_Plot(bf,{'ux','uy'},'xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');
pause(0.1);

%% Chapter 3 : plot forces (reads from database)

sfs = SF_Status('all'); % generates summary of database
Re_tab = [sfs.MESHES.Re];
Fx_tab = [sfs.MESHES.Fx];
Fy_tab = [sfs.MESHES.Fy];
figure; semilogx(Re_tab,Fx_tab,'-r');xlabel('Re');ylabel('Fx,Fy');
hold on; semilogx(Re_tab,Fy_tab,'--b');legend('Fx','Fy');



%% Chapter 4 ; demonstration of several mesh-adaptation strategies
% 
% we restart from the previously computed BF for Re=2000
bf=SF_BaseFlow(bf,'Re',2000,'alpha',alpha);

%%
% try a nearly isotropic mesh with progressive derefinement
bfA=SF_Adapt(bf,'Hmax',HMax,'anisomax',1,'nbjacoby',10,'recompute',false);
disp([' Number of cells in this mesh : ',num2str(bfA.mesh.np)]); 
    figure; subplot(2,2,1);SF_Plot(bfA,'mesh','title','Isotropic','xlim',[-2 4],'ylim',[-2 2]); 
    subplot(2,2,2);SF_Plot(bfA,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');
    subplot(2,2,3);SF_Plot(bfA,'mesh','xlim',[-.1 .1],'ylim', [-.1 .1],'boundary','on');
    subplot(2,2,4);SF_Plot(bfA,'mesh','xlim',[.9 1.1],'ylim', [-.1 .1],'boundary','on');


%%
% try a very anisotropic mesh
bfA=SF_Adapt(bf,'Hmax',HMax,'anisomax',1000,'nbjacoby',1,'recompute',false);
disp([' Number of cells in this mesh : ',num2str(bfA.mesh.np)]); 
    figure; subplot(2,2,1);SF_Plot(bfA,'mesh','title','Anisotropic','xlim',[-2 4],'ylim',[-2 2]); 
    subplot(2,2,2);SF_Plot(bfA,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');
    subplot(2,2,3);SF_Plot(bfA,'mesh','xlim',[-.1 .1],'ylim', [-.1 .1],'boundary','on');
    subplot(2,2,4);SF_Plot(bfA,'mesh','xlim',[.9 1.1],'ylim', [-.1 .1],'boundary','on');


%%
% try adaptation mask
Mask = SF_Mask(bf,[-.1 1.1 -.1 .1 .05]); % this generates a mask corresponding to an 'inner box' x=[-.1,1.1],y= [-.1,.1] with max grid size 0.05
Mask2 = SF_Mask(bf,[-.5 2 -.5 .5 .2]); % this generates a mask corresponding to an 'intermadiate box' x=[-.5,2],y= [-.5,.5] with max grid size 0.2
bfA=SF_Adapt(bf,Mask,Mask2,'Hmax',1); % adapt to bf and mask (max grid size is 1 in outer domain)
disp([' Number of cells in this mesh : ',num2str(bfA.mesh.np)]); 
if (bfA.iter>0)
    figure; subplot(2,2,1);SF_Plot(bfA,'mesh','title','Adapt with mask','xlim',[-2 4],'ylim',[-2 2]); 
    subplot(2,2,2);SF_Plot(bfA,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');
    subplot(2,2,3);SF_Plot(bfA,'mesh','xlim',[-.1 .1],'ylim', [-.1 .1],'boundary','on');
    subplot(2,2,4);SF_Plot(bfA,'mesh','xlim',[.9 1.1],'ylim', [-.1 .1],'boundary','on');
end

%%
% try adaptation to quasi-static forced structure
Forced = SF_LinearForced(bf,'omega',1e-6);
disp([' Number of cells in this mesh : ',num2str(bfA.mesh.np)]); 
bfA=SF_Adapt(bf,Forced,'Hmax',HMax);
if (bfA.iter>0)
    figure; subplot(2,2,1);SF_Plot(bfA,'mesh','title','Adapt to QS structure','xlim',[-2 4],'ylim',[-2 2]); 
    subplot(2,2,2);SF_Plot(bfA,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');
    subplot(2,2,3);SF_Plot(bfA,'mesh','xlim',[-.1 .1],'ylim', [-.1 .1],'boundary','on');
    subplot(2,2,4);SF_Plot(bfA,'mesh','xlim',[.9 1.1],'ylim', [-.1 .1],'boundary','on');
end

%%
% Combination of all these ideas
Forced = SF_LinearForced(bf,'omega',0);
Mask = SF_Mask(bf,[-.5 2 -.5 .5 .1]);
Mask2 = SF_Mask(bf,[-.5 2 -.5 .5 .2]);
bfA=SF_Adapt(bf,Mask,Mask2,Forced,'anisomax',100);
disp([' Number of cells in this mesh : ',num2str(bfA.mesh.np)]); 
if (bfA.iter>0)
    figure; subplot(2,2,1);SF_Plot(bfA,'mesh','title','Combining ideas','xlim',[-2 4],'ylim',[-2 2]); 
    subplot(2,2,2);SF_Plot(bfA,'mesh','xlim',[-.2 1.2],'ylim', [-.4 .4],'boundary','on');
    subplot(2,2,3);SF_Plot(bfA,'mesh','xlim',[-.1 .1],'ylim', [-.1 .1],'boundary','on');
    subplot(2,2,4);SF_Plot(bfA,'mesh','xlim',[.9 1.1],'ylim', [-.1 .1],'boundary','on');
end

%%
% Summary of all has been done so far

sfs = SF_Status;

%% APPENDIX : how does this work and how to do this outside of Matlab/Octave
%
% If you don't have (or don't like) Octave/Matlab, good news ! you can
% perfectly do all the computation by directly launching the FreeFem solvers in a bash
% terminal.
%
%
% Here is how to compute the initial mesh, and compute the base flow while
% increasing Reynolds number :
%
%   echo  30  128   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Naca0012MeshFull.edp  
%   mv ./WORK/mesh.msh ./WORK/MESHES/FFMESH_1.msh
%   cp ./WORK/MESHES/FFMESH_1.msh ./WORK/mesh.msh
%   echo 10 | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp -alpha 10 
%   cp ./WORK/BaseFlow.txt ./WORK/BASEFLOWS/BaseFlow_Re10_Omegax0.txt
%   cp ./WORK/BASEFLOWS/BaseFlow_Re10_Omegax0.txt ./WORK/BaseFlow_guess.txt
%   echo 20 | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp -alpha 10 
%   (...)
%
%
% And here is how to do the composite mesh adaptation of the last example :
%
%   echo rectangle -0.5 2 -0.5 0.5 0.2 | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 AdaptationMask.edp  
%   cp ./WORK/Mask.txt ./WORK/FlowFieldToAdapt2.txt
%   echo rectangle -0.1 1.1 -0.1 0.1 0.05 | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 AdaptationMask.edp  
%   cp ./WORK/Mask.txt ./WORK/FlowFieldToAdapt3.txt
%   echo  array 1 0 0 | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 LinearForced2D.edp
%   cp ./WORK/ForcedFlow.txt ./WORK/FlowFieldToAdapt4.txt
%   cp ./WORK/FORCEDFLOWS/Field_Impedance_Re2000_Omega0.ff2m ./WORK/FlowFieldToAdapt3.ff2m
%   cp ./WORK/BaseFlow.txt ./WORK/FlowFieldToAdapt1.txt
%   #  Here a file  Param_Adaptmesh.idp has been created by driver SF_Adapt 
%   echo  4  ReP2P2P1 1  CxP2P2 0 xP2P2 0 CxP2P2P1 0  | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 AdaptMesh.edp  
%   mv ./WORK/mesh_adapt.msh ./WORK/MESHES/FFMESH_14.msh
%   cp ./WORK/FlowFieldAdapted1.txt ./WORK/MISC/BaseFlow_ADAPTED_NO_RECOMPUTED.txt
%   cp ./WORK/MISC/BaseFlow_ADAPTED_NO_RECOMPUTED.txt ./WORK/BaseFlow_guess.txt
%   echo 3000 | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp -alpha 10 
%
%
% A full summary of all the bash commands launched by this script is written in the .stabfem_log.txt file.
% We recommend you to have a look at this file to understand how it works !
%
% [[PUBLISH]]