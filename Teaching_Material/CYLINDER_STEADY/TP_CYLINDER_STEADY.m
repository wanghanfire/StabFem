%%  STEADY FLOW - TUTORIAL 2 FOR MSc 1st year in Paul Sabatier University
%
%  This script has been designed to study the flow around a cylinder 
%  at low Reynolds number [1-40]. 
%  We study how this flow is characterised by analysing main properties
%  of flow quantities such as flow field, pressure, vorticity and
%  shear stress.
%  
%  The script performs the following calculations :
% 
%  # Generation of a mesh
%  # Computation of the stationary solution at Re = [1-40]
%  # Display flow fields
%

%%
%
% First we set a few global variables needed by StabFem
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity', 4);

%% Parameters of the simulation
% The mesh is generated with a domain size of [xmin, xmax, ymax] 
% xmin: Distance from the cylinder center to the inlet
% xmax: Distance from the cylinder center to the outlet
% ymax: Distance from the axis of symmetry to the upper boundary.

xmin = -40; xmax = 40; ymax = 80;
Re = 40;
% In this first TP we will deal with the steady flow past a circular cylinder
% which is the continuation of this series of TP. This means that
% we will consider Re=[1,40].


%% build mesh and compute steady-state solution
% We compute an initial mesh witCx = trapz(theta, 0.5*((-tauxxwall.*C - tauxywall.*S)/Re + pWall.*C));

% Then we compute a steady-state with the command SF_BaseFlow and then we
% perform mesh adaptation. The mesh adaptation algorithm refines the mesh
% wherever the steady state, which is passed as an input to SF_Adapt, have
% high gradients. 

ffmesh = SF_Mesh('Mesh_Cylinder_FullDomain.edp','Params',[xmin xmax ymax],'problemtype','2D','symmetry','N');
bf=SF_BaseFlow(ffmesh,'Re',Re,'type','NEW');
bf = SF_Adapt(bf,'InterpError',5e-3);

% NB for speed-up you may replace this sequence of commands by 
% bf = SF_Load('MESHES','last')
% (proviced you have previously generated the mesh)

%% plot Ux, Uy, omega, P
% Il montre le dernier baseflow. Ici vous pouvez afficher vos champs.  
figure();
subplot(2,2,1); SF_Plot(bf,'ux','xlim',[-3 5],'ylim',[-3 3],'title','u_x'); hold on;
SF_Plot(bf,'psi','Contour','only','CLevels',[-0.05:0.01:0.05],'CColor','w','xlim',[-3 5],'ylim',[-3 3]);
subplot(2,2,2); SF_Plot(bf,'uy','xlim',[-3 5],'ylim',[-3 3],'title','u_y'); hold on;
SF_Plot(bf,'psi','Contour','only','CLevels',[-0.05:0.01:0.05],'CColor','k','xlim',[-3 5],'ylim',[-3 3]);
subplot(2,2,3); SF_Plot(bf,'vort','Contour','on','CLevels',[-1,-0.5,-0.02,0.02,0.5,1],'xlim',[-3 5],'ylim',[-3 3],'title','\omega','colorrange',[-1,1]);
subplot(2,2,4); SF_Plot(bf,'p','Contour','on','xlim',[-3 5],'ylim',[-3 3],'title','p');

%% Quiver plot
bf.Normu = sqrt(bf.ux.^2+bf.uy.^2);
% figure ; SF_Plot(bf,'Normu')

figure;
SF_Plot(bf,'Normu','xlim',[-1 5],'ylim',[-3 3],'title','u_x');
hold on;
SF_Plot(bf,'psi','Contour','only','CLevels',[-0.05:0.01:0.05],'CColor','y','xlim',[-3 5],'ylim',[-3 3]);
SF_Plot(bf,{'ux','uy'},'xlim',[-1 5],'ylim',[-3 3],'FColor','w');

%% If you want to plot the norm of the velocity

% Velocity plots

% tout le long du cylindre (RMin = 0.5). Vous pouvez modifier la valeur de
% RMin pour ainsi analyser d'un point de vue local l'écoulement autour le
% cylindre
RMax = 50; RMin = 0.5; % RMin et RMax sont modifiables. Rappelez vous RMax <= min(xmax,ymax)
R = [RMin:.01:RMax];
thetaList = [pi/12:pi/12:pi/2];  
nx = size(thetaList); nx = nx(2);
ny = size(R); ny = ny(2);

UBLVec = zeros(nx,ny);
VBLVec = zeros(nx,ny);
UrBLVec = zeros(nx,ny);
UthBLVec = zeros(nx,ny);

for index=[1:nx]
    theta = thetaList(index);
    X=R.*cos(theta); Y=R.*sin(theta);
    V=SF_ExtractData(bf,'uy',X,Y); VBLVec(index,:) = V;
    U=SF_ExtractData(bf,'ux',X,Y); UBLVec(index,:) = U;
    UrBL = U.*cos(theta) + V.*sin(theta); UrBLVec(index,:) = UrBL;
    UthBL = -U.*sin(theta) + V.*cos(theta); UthBLVec(index,:) = UthBL;
end

% U_theta - Couche limite du cylindre (coord polaires)
figure;
hold on;
for index=[1:nx]
    V = UthBLVec(index,:);
    legendName = ['$\theta = ',num2str(index),'\pi/12 $'];
    plot(V,R,'o','DisplayName',legendName); 
    xlabel('$U_{\theta}(x,y)$','Interpreter','latex'); 
    ylabel('$r$','Interpreter','latex');
    title('$U_{\theta}(r,\theta)$', 'Interpreter','latex');
end
leg = legend;
set(leg,'Interpreter','latex','fontsize',24);
ylim([0,4]);

%%
% Calcul de la contrainte parietale

theta = linspace(0,2*pi,200)
Ycircle = 0.5*sin(theta); Xcircle = 0.5*cos(theta); 

tauxxwall = SF_ExtractData(bf,'tauxx',Xcircle,Ycircle);
tauxywall = SF_ExtractData(bf,'tauxy',Xcircle,Ycircle);
tauyywall = SF_ExtractData(bf,'tauyy',Xcircle,Ycircle);
S = sin(theta); C = cos(theta);
tauWallN =  C.*(tauxxwall.*C+tauxywall.*S) + S.*(tauxywall.*C+tauyywall.*S);
tauWallT = - S.*(tauxxwall.*C+tauxywall.*S) + C.*(tauxywall.*C+tauyywall.*S);
pWall = SF_ExtractData(bf,'p',Xcircle,Ycircle); 

figure;
plot(theta,tauWallT,'b^');
hold on;
plot(theta,tauWallN,'r^');
xlabel('\theta'); ylabel('\tau_{w}'); title('shear stress $\tau(r=R,\theta)$ along the surface', 'Interpreter','latex');
legend('$Re=40$','Interpreter','latex');


% Calcul de la traînée
Cx = 2*trapz(theta, 0.5*((tauWallN.*C - tauWallT.*S)/Re - pWall.*C));



