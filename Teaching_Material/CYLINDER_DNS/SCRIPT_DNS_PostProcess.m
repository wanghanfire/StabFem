%%  DNS FLOW - TUTORIAL 3 FOR MSc 1st year in Paul Sabatier University
%
% This example shows how to use StabFem to plot results from a DNS.
% The present script performs only the postprocessing and assumes you have
% a previously generated dataset in the directory './WORK/'. 
% 
% To see how to perform the full DNS please chech the script "SCRIPT_DNS_EXAMPLE.m"

%% Chapter 0 : initialization
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity', 4,'ffdatadir', './WORK/');

% Check what is available in './WORK/'
sfs = SF_Status('ALL')

% Normally you have imported 10 instants 


%% Chapter 1 : visualization of the flow at a specific instant
%
% First load the 10 snapshots

for isnap = 1:10
    DNSfields(isnap) = SF_Load('DNSFIELDS',isnap);
end

% alternative method
%load('DNSfields.mat')


% Then plots as you have learned

figure; SF_Plot(DNSfields(1),'vort','xlim',[-2 4],'ylim',[-3 3]);

% we plot the pressure and vorticity along the surface
alpha = linspace(-pi,pi,201);
Xsurf = .501*cos(alpha);
Ysurf = .501*sin(alpha);
Psurf = SF_ExtractData(DNSfields(1),'p',Xsurf,Ysurf);
Omegasurf = SF_ExtractData(DNSfields(1),'vort',Xsurf,Ysurf);
figure(16);
subplot(2,1,1);title('Pressure along the surface P(r=a,\theta) at final time step')
plot(alpha,Psurf);
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);title('Vorticity along the surface \omega(r=a,\theta) at final time step')
plot(alpha,Omegasurf);
xlabel('\theta');ylabel('\omega_z(r=a,\theta)');


%% Chapter 2 : plot the lift and drag forces 

% We now plot the lift force as function of time
% Everything is contained in the object sfs.DNSSTATS ; have a look to understand !

%figure(15);
%subplot(2,1,1);
%plot(sfs.DNSSTATS.t,sfs.DNSSTATS.Fy);title('Lift force as function of time');
%xlabel('t');ylabel('Fy');
%subplot(2,1,2);
%plot(sfs.DNSSTATS.t,sfs.DNSSTATS.Fx);title('Drag force as function of time');
%xlabel('t');ylabel('Fx');

figure(15);
subplot(2,1,1);
plot(sfs.DNSSTATS.t,sfs.DNSSTATS.Fy,[sfs.DNSFIELDS.t],[sfs.DNSFIELDS.Fy],'o');title('Lift force as function of time');
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(sfs.DNSSTATS.t,sfs.DNSSTATS.Fx,[sfs.DNSFIELDS.t],[sfs.DNSFIELDS.Fx],'o');title('Drag force as function of time');
xlabel('t');ylabel('Fx');

%% BONUS : howq to generate a movie 
% 
 



% Here is how to generate a short movie
h = figure;
filename = 'DNS_Cylinder_Re60.gif';
for i=1:10
    SF_Plot(DNSfields(i),'ux','xlim',[-2 10],'ylim',[-3 3 ],'colorrange','cropmaxmin',...
        'title',['t = ',num2str(DNSfields(i).t)],'boundary','on','bdlabels',2);
    hold on; SF_Plot(DNSfields(i),'psi','contour','only','clevels',[-2 -1.5 -1 -.5  -.2 -.1 -0.05 -.02 0 0.02 0.05 0.1 .2 .5 1 1.5 2],'xlim',[-2 10],'ylim',[-3 3]);
    hold off;
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<DNS_Cylinder_Re60.gif>>
%

