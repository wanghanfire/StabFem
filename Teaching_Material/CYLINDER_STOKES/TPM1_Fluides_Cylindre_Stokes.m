%%  STOKES FLOW - TUTORIAL 1 FOR MSc 1st year in Paul Sabatier University
%
%  This script has been designed to study the flow around a cylinder 
%  at very low Reynolds number. 
%  We study how this flow is characterised by analysing main properties
%  of flow quantities such as flow field, pressure, vorticity and
%  shear stress.
%
%  
%  The script performs the following calculations :
% 
%  # Generation of a mesh
%  # Computation of the stationary solution at Re = 1, Re = 0.1 
%  and Re = 0.05 via Newton method
%  # Display flow fields
%  # Display velocity profiles along y and x
%  # Display pressure
%  # Display drag coefficient and comparison with theory
%  # Display shear stress
%

%%
%
% First we set a few global variables needed by StabFem
%
% (NB to understand the usage of the 'workdir' option see chapter 5 of this script)

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity', 4);

%% Parameters of the simulation
% The mesh is generated with a domain size of [xmin, xmax, ymax] 
% xmin: Distance from the cylinder center to the inlet
% xmax: Distance from the cylinder center to the outlet
% ymax: Distance from the axis of symmetry to the upper boundary.
% Please consider that only half of the computational domain is simulated.
% The flow past a circular infinite cylinder is symmetric with respect
% to the x-axis up to more or less Re=47

xmin = -60; xmax = 60; ymax = 500;

% In this first TP we will deal with the Stokes flow. This means that
% we will consider Re << 1. However, very low Reynolds require large
% computational domains, which means that we need more time to run
% computations. Then we need to find a compromise, the compromise is to set
% a small Re but not too small.

Re = 0.25;
%% build mesh and compute steady-state solution
% We compute an initial mesh with the command SF_Mesh
% Then we compute a steady-state with the command SF_BaseFlow and then we
% perform mesh adaptation. The mesh adaptation algorithm refines the mesh
% wherever the steady state, which is passed as an input to SF_Adapt, have
% high gradients. 
ffmesh=SF_Mesh('Mesh_Cylinder.edp','Params',[xmin xmax ymax],'problemtype','2D','symmetry','S');
bf=SF_BaseFlow(ffmesh,'Re',1);
bf = SF_Adapt(bf,'Hmax',30,'InterpError',1e-3);
bf=SF_BaseFlow(bf,'Re',Re);
bf01 = bf;


%% Plot the computed mesh

figure();SF_Plot(bf,'mesh','title','mesh');
pauseKey;
%% plot Ux, Uy, omega, P

figure(1);
subplot(2,2,1); SF_Plot(bf01,'ux','Contour','on','xlim',[-3 3],'ylim',[-3 3],'title','u_x','symmetry','XS');
subplot(2,2,2); SF_Plot(bf01,'psi','Contour','on','xlim',[-3 3],'ylim',[-3 3],'title','\psi','colorrange','cropminmax','symmetry','XA');
subplot(2,2,3); SF_Plot(bf01,'vort','Contour','on','xlim',[-3 3],'ylim',[-3 3],'title','\omega','symmetry','XA');
subplot(2,2,4); SF_Plot(bf01,'p','Contour','on','xlim',[-3 3],'ylim',[-3 3],'title','p','symmetry','XS');
pauseKey;
%% Streamlines 

figure;
subplot(1,2,1); SF_Plot(bf01,'psi','Contour','on','xlim',[-4.5 4.5],'ylim',[0 3],'title','\psi','colorrange','cropminmax');
subplot(1,2,2); SF_Plot(bf01,'psi','Contour','on','xlim',[-20 20],'ylim',[-20 20],'title','\psi','symmetry','XA','colorrange','cropminmax');
pauseKey;

%% Velocity profiles along y (Vitesse tout au long y)
component = "uy"; % Vous pouvez choisir 'ux' ou 'uy'



Yvert = [0.5:.01:ymax]; 
Uvert01 = SF_ExtractData(bf01,component,0,Yvert);
ReText = "$Re="+num2str(Re)+"$";

% Wake (sillage)
xWake = 1;
YvertW = [0.0:.01:ymax]; 
UvertW01 = SF_ExtractData(bf01,component,xWake,YvertW);

if(component == "ux")
    UvertW01S = UvertW01;
    Utext = '$U$';
elseif(component=="uy")
    UvertW01S = -UvertW01;
    Utext = '$V$';
end

figure;
plot(Uvert01,Yvert,'ko-');
xlabel(Utext,'Interpreter','latex'); 
ylabel('$y$','Interpreter','latex'); 
title('Velocity $U(0,y)$ along vertical line', 'Interpreter','latex');
legend(ReText,'Interpreter','latex');
pauseKey;

figure;
plot(UvertW01,YvertW,'b^-');
xlabel(Utext,'Interpreter','latex'); 
ylabel('$y$','Interpreter','latex'); 
title('Velocity $U(1,y)$ along vertical line', 'Interpreter','latex');
legend(ReText,'Interpreter','latex');
ylim([0,10]);
pauseKey;

figure;
plot(UvertW01,YvertW,'b^-');
xlabel(Utext,'Interpreter','latex'); 
ylabel('$y$','Interpreter','latex'); 
title('Velocity $U(1,y)$ all along a vertical line', 'Interpreter','latex');
legend(ReText,'Interpreter','latex');
ylim([0,ymax]);
pauseKey;

%% Velocity profiles along x (Vitesse tout au long x)
% Parameters to play with! :)
component = "ux"; % Vous pouvez choisir 'ux' ou 'uy'
yCoord = 1; % Vous pouvez explorer l'ecoulement si tu me modifies :)

if(component == "ux")
    Utext = '$U$';
elseif(component=="uy")
    Utext = '$V$';
end

Xcoord = [0.5:.01:20]; 
UvertW01 = SF_ExtractData(bf01,component,Xcoord,yCoord);
Uvert01 = SF_ExtractData(bf01,component,Xcoord,0);


figure;
plot(Xcoord,Uvert01,'ko-');
xlabel(Utext,'Interpreter','latex'); 
ylabel('$x$','Interpreter','latex'); 
title('Velocity $U(x,0)$ along vertical line', 'Interpreter','latex');
legend(ReText,'Interpreter','latex');
pauseKey;

% Wake (sillage)
figure;
plot(Xcoord,UvertW01,'ko-');
xlabel(Utext,'Interpreter','latex'); 
ylabel('$x$','Interpreter','latex'); 
title("Velocity $U(x,"+yCoord+")$ along vertical line", 'Interpreter','latex');
legend(ReText,'Interpreter','latex');
pauseKey;


%% pressure along the surface

figure;
theta = linspace(0,pi,1000)
Ycircle = 0.5*sin(theta); Xcircle = 0.5*cos(theta); 
P2 = SF_ExtractData(bf01,'p',Xcircle,Ycircle);
plot(theta,P2,'b^');
xlabel('\theta'); ylabel('p'); title('pressure $p(r=R,\theta)$ along the surface', 'Interpreter','latex');
legend(ReText,'Interpreter','latex');
pauseKey;


%% Vorticity 
figure;
SF_Plot(bf01,'vort','Contour','on','xlim',[-5 5],'ylim',[-5 5],'title',ReText,'symmetry','XA','colorrange',[-1,1]);
pauseKey;

%Vorticité tout au long la paroi
figure;
theta = linspace(0,pi,5000)
Ycircle = 0.5*sin(theta); Xcircle = 0.5*cos(theta); 
VortWall = SF_ExtractData(bf01,'vort',Xcircle,Ycircle);
plot(theta,VortWall,'b^');
xlabel('\theta'); ylabel('\omega'); title('$\omega(r=R,\theta)$ along the surface', 'Interpreter','latex'); 
legend(ReText,'Interpreter','latex');
pauseKey;

%% Drag coefficient
% Theoretical first order prediction (Van Dyke)
gamma = 0.5772156649015328606065120900824024310421 % Euler-Mascheroni const

delta = 1.0/(0.5-gamma-log(Re/4));
Cd01 = 4*pi/Re*(delta - 0.87*delta^3);

ReList = [Re];
CdTh = [Cd01];
CdNum = [ bf01.Fx];

errorRel = (Cd01-bf01.Fx)/(Cd01);

plot(ReList,CdTh,'rs', ReList,CdNum,'ko');
xlabel('$Re$','Interpreter','latex'); 
ylabel('$C_d$','Interpreter','latex'); 
title('Drag coefficient $C_d$','Interpreter','latex');
legend('Theoretical','Numerical','Interpreter','latex');
pauseKey;


%% shear along the surface
theta = linspace(0,pi,1000)
Ycircle = 0.5*sin(theta); Xcircle = 0.5*cos(theta); 
tau = SF_ExtractData(bf01,'tauxy',Xcircle,Ycircle);

figure;
plot(theta,tau,'b^');
xlabel('\theta'); ylabel('\tau'); title('shear stress $\tau(r=R,\theta)$ along the surface', 'Interpreter','latex');
legend('$Re=0.1$','Interpreter','latex');
pauseKey;


theta = linspace(0,pi,1000)
Ycircle = 0.5*sin(theta); Xcircle = 0.5*cos(theta); 
tauWall = SF_ExtractData(bf01,'tauWall',Xcircle,Ycircle);
pauseKey;

figure;
plot(theta,tauWall,'b^');
xlabel('\theta'); ylabel('\tau_{w}'); title('shear stress $\tau(r=R,\theta)$ along the surface', 'Interpreter','latex');
legend('$Re=0.1$','Interpreter','latex');


